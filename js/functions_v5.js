// variables iniciales
var initialData;
var daysOfYear = 365;
var waccB4 = 0.01;
var fcfB19 = 0;
var fcfB21 = 0;
/**
 * devuelve el valor del tir
 * @param T inversion
 * @param T1 retorno primer ciclo
 * @param T2 retorno segundo ciclo
 * @param T3 retorno tercer ciclo
 * @param T4 retorno cuarto ciclo
 * @param T5 retorno quinto ciclo
 * @returns Promise valor degerminado del tir
 */
function getTir(T, T1, T2, T3, T4, T5) {
    return new Promise(function (resolve) {
        for (var tir = 0; tir <= 100; tir += 0.0001) {
            var tirFixed = Number(tir.toFixed(4));
            var van = T + (T1 / (1 + tirFixed)) +
                (T2 / Math.pow((1 + tirFixed), 2)) +
                (T3 / Math.pow((1 + tirFixed), 3)) +
                (T4 / Math.pow((1 + tirFixed), 4)) +
                (T5 / Math.pow((1 + tirFixed), 5));
            if (Math.round(van) === 0) {
                resolve(tirFixed);
                break;
            }
        }
    });
}
/**
 * calcula el tir modificado
 * @param T
 * @param T1
 * @param T2
 * @param T3
 * @param T4
 * @param T5
 * @returns {Promise}
 */
function getTirModificate(T, T1, T2, T3, T4, T5, K) {
    return new Promise(function (resolve) {
        var x = Math.abs(T);
        var y = 0;
        var tK = 1 + K;
        if (T1 < 0) {
            console.log('t1 negativo');
            x += (Math.abs(T1) / Math.pow(tK, 1));
        } else {
            console.log('t1 positivo');
            y += (T1 * Math.pow(tK, 4));
        }
        if (T2 < 0) {
            console.log('t2 negativo');
            x += (Math.abs(T2) / Math.pow(tK, 2));
        } else {
            console.log('t2 positivo');
            y += (T2 * Math.pow(tK, 3));
        }
        if (T3 < 0) {
            console.log('t3 negativo');
            x += (Math.abs(T3) / Math.pow(tK, 3));
        } else {
            console.log('t3 positivo');
            y += (T3 * Math.pow(tK, 2));
        }
        if (T4 < 0) {
            console.log('t4 negativo');
            x += (Math.abs(T4) / Math.pow(tK, 4));
        } else {
            console.log('t4 positivo');
            y += (T4 * Math.pow(tK, 1));
        }
        if (T5 < 0) {
            console.log('t4 negativo');
            x += (Math.abs(T5) / Math.pow(tK, 5));
        } else {
            console.log('t5 positivo');
            y += (T5 * Math.pow(tK, 0));
        }
        console.log(x,y);
        var TIRM = 1 - Math.pow(y / x, 1 / 5);
        resolve(Math.abs(TIRM));
    });
}

/**
 * calcula los datos iniciales
 */
function calculateData() {
    return new Promise(function (resolve) {
        var initialDataC4 = initialData.initialDataC4;
        var initialDataC5 = initialData.initialDataC5;
        var initialDataC6 = initialData.initialDataC6;
        var initialDataC9 = initialData.initialDataC9;
        var initialDataC12 = initialData.initialDataC12;
        var initialDataC17 = initialData.initialDataC17;
        var initialDataC18 = initialData.initialDataC18;
        var initialDataC20 = initialData.initialDataC20;
        var initialDataC21 = initialData.initialDataC21;
        var initialDataC23 = initialData.initialDataC23;
        var initialDataC35 = initialData.initialDataC35;

        /**
         * Variables iniciales de Assumptions
         */
        var assumptionsB2 = initialData.assumptionsB2;
        var assumptionsB7 = initialData.assumptionsB7;
        var assumptionsB16 = initialData.assumptionsB16;
        var assumptionsB17 = initialData.assumptionsB17;
        var assumptionsB22 = initialData.assumptionsB22;
        var assumptionsB34 = initialData.assumptionsB34;
        var assumptionsB35 = initialData.assumptionsB35;
        var assumptionsB36 = initialData.assumptionsB36;
        var assumptionsB37 = initialData.assumptionsB37;
        var assumptionsB38 = initialData.assumptionsB38;
        var assumptionsB39 = initialData.assumptionsB39;
        var assumptionsB40 = initialData.assumptionsB40;

        var assumptionsC14 = initialData.assumptionsC14;
        var assumptionsC15 = initialData.assumptionsC15;
        var assumptionsC19 = initialData.assumptionsC19;
        var assumptionsC20 = initialData.assumptionsC20;
        var assumptionsC21 = initialData.assumptionsC21;
        var assumptionsC28 = initialData.assumptionsC28;
        var assumptionsC29 = initialData.assumptionsC29;
        var assumptionsC32 = initialData.assumptionsC32;

        var assumptionsD11 = initialData.assumptionsD11;
        var assumptionsD14 = initialData.assumptionsD14;
        var assumptionsD15 = initialData.assumptionsD15;
        var assumptionsD19 = initialData.assumptionsD19;
        var assumptionsD20 = initialData.assumptionsD20;
        var assumptionsD21 = initialData.assumptionsD21;
        var assumptionsD28 = initialData.assumptionsD28;
        var assumptionsD29 = initialData.assumptionsD29;
        var assumptionsD32 = initialData.assumptionsD32;

        var assumptionsE11 = initialData.assumptionsE11;
        var assumptionsE14 = initialData.assumptionsE14;
        var assumptionsE15 = initialData.assumptionsE15;
        var assumptionsE19 = initialData.assumptionsE19;
        var assumptionsE20 = initialData.assumptionsE20;
        var assumptionsE21 = initialData.assumptionsE21;
        var assumptionsE28 = initialData.assumptionsE28;
        var assumptionsE29 = initialData.assumptionsE29;
        var assumptionsE32 = initialData.assumptionsE32;

        var assumptionsF11 = initialData.assumptionsF11;
        var assumptionsF14 = initialData.assumptionsF14;
        var assumptionsF15 = initialData.assumptionsF15;
        var assumptionsF19 = initialData.assumptionsF19;
        var assumptionsF20 = initialData.assumptionsF20;
        var assumptionsF21 = initialData.assumptionsF21;
        var assumptionsF28 = initialData.assumptionsF28;
        var assumptionsF29 = initialData.assumptionsF29;
        var assumptionsF32 = initialData.assumptionsF32;

        var assumptionsG11 = initialData.assumptionsG11;
        var assumptionsG14 = initialData.assumptionsG14;
        var assumptionsG15 = initialData.assumptionsG15;
        var assumptionsG19 = initialData.assumptionsG19;
        var assumptionsG20 = initialData.assumptionsG20;
        var assumptionsG21 = initialData.assumptionsG21;
        var assumptionsG28 = initialData.assumptionsG28;
        var assumptionsG29 = initialData.assumptionsG29;
        var assumptionsG32 = initialData.assumptionsG32;

        /**
         * Variables iniciales de EVCforShareholders
         */
        var evcForShareholdersB13 = initialData.evcForShareholdersB13;
        var evcForShareholdersB14 = initialData.evcForShareholdersB14;


        /** /////////////////////////
         *  initial data
         */
        /** bloque initial balance */
        var initialDataC36 = assumptionsB22;
        var initialDataC37 = initialDataC36 * initialDataC17;

        // operational cash
        $('#initialDataC7').val(initialDataC37.toFixed(1));
        // total operat wc
        var initialDataC7 = initialDataC37;
        var initialDataC8 = initialDataC4 + initialDataC5 + initialDataC6 + initialDataC7;
        $('#initialDataC8').val(initialDataC8.toFixed(1));

        // total net assets
        var initialDataC10 = initialDataC8 + initialDataC9;
        $('#initialDataC10').val(initialDataC10.toFixed(1));
        // D+E
        var initialDataC14 = initialDataC10;
        $('#initialDataC14').val(initialDataC14.toFixed(1));
        // Book Value Equity
        var initialDataC13 = initialDataC14 - initialDataC12;
        $('#initialDataC13').val(initialDataC13.toFixed(1));

        /** bloque p&l */
            // p & l Sales
        // algo aqui var initialDataD17 = Math.abs(initialDataC17 / initialDataC17);
        var initialDataD17 = 1;
        $('#initialDataD17').html((initialDataD17 * 100).toFixed(1) + "%");
        // p & l cgs
        var initialDataD18 = Math.abs(initialDataC18 / initialDataC17);
        $('#initialDataD18').html((initialDataD18 * 100).toFixed(1) + "%");

        // gross margin
        var initialDataC19 = initialDataC17 + initialDataC18;
        $('#initialDataC19').val(initialDataC19);
        // p & l gross margin
        var initialDataD19 = Math.abs(initialDataC19 / initialDataC17);
        $('#initialDataD19').html((initialDataD19 * 100).toFixed(1) + "%");
        // p & l opex
        var initialDataD20 = Math.abs(initialDataC20 / initialDataC17);
        $('#initialDataD20').html((initialDataD20 * 100).toFixed(1) + "%");
        // p & l deprecation
        var initialDataD21 = Math.abs(initialDataC21 / initialDataC17);
        $('#initialDataD21').html((initialDataD21 * 100).toFixed(1) + "%");
        // EBIT
        var initialDataC22 = initialDataC19 + initialDataC20 + initialDataC21;
        $('#initialDataC22').val(initialDataC22.toFixed(1));
        // p & l EBIT
        var initialDataD22 = initialDataC22 / initialDataC17;
        $('#initialDataD22').html((initialDataD22 * 100).toFixed(1) + "%");
        // p & l Taxes
        var initialDataD23 = Math.abs(initialDataC23 / initialDataC22);
        $('#initialDataD23').html((initialDataD23 * 100).toFixed(1) + "%");
        // EBIaT
        var initialDataC24 = initialDataC22 + initialDataC23;
        $('#initialDataC24').val(initialDataC24.toFixed(1));
        // p & l EBIaT
        var initialDataD24 = Math.abs(initialDataC24 / initialDataC17);
        $('#initialDataD24').html((initialDataD24 * 100).toFixed(1) + "%");

        /** bloque days of... */
            // days of S A/R
        var initialDataC27 = initialDataC4 * daysOfYear / initialDataC17;
        $('#initialDataC27').val(initialDataC27.toFixed(1));
        // days of Sales Invent
        var initialDataC28 = -1 * initialDataC5 * daysOfYear / initialDataC18;
        $('#initialDataC28').val(initialDataC28.toFixed(1));
        // days of Sales  AccP
        var initialDataC29 = initialDataC6 * daysOfYear / initialDataC18;
        $('#initialDataC29').val(initialDataC29.toFixed(1));

        // EV at book value
        var initialDataC32 = initialDataC10 * 1;
        $('#initialDataC32').val(initialDataC32.toFixed(1));

        // Operational Cash
        $('#initialDataC36').val((initialDataC36 * 100).toFixed(0));
        // Operational cash included
        $('#initialDataC37').val(initialDataC37.toFixed(0));
        // Cash in Excess
        var initialDataC38 = initialDataC35 - initialDataC37;
        $('#initialDataC38').val(initialDataC38.toFixed(0));


        /**
         * calcula Assumptions
         */
        /** /////////////////////////
         *  Assumptions
         */

        // T1-T5 FILA 12
        var assumptionsC11 = assumptionsB2;
        $('#assumptionsC11').val(assumptionsC11.toFixed(1));

        // sales radio button
        var assumptionsC1 = Number($("input[name='assumptionsC1']:checked").val());

        var assumptionsC12 = '';
        var assumptionsD12 = '';
        var assumptionsE12 = '';
        var assumptionsF12 = '';
        var assumptionsG12 = '';

        if (assumptionsC1 === 2) {
            // Cumulative Growth of sales in %
            // Calculos para T1-T5 FILA 12
            assumptionsC12 = (assumptionsB2).toFixed(1);
            assumptionsD12 = (assumptionsC12 * (1 + assumptionsB7)).toFixed(1);
            assumptionsE12 = (assumptionsD12 * (1 + assumptionsB7)).toFixed(1);
            assumptionsF12 = (assumptionsE12 * (1 + assumptionsB7)).toFixed(1);
            assumptionsG12 = (assumptionsF12 * (1 + assumptionsB7)).toFixed(1);
        }

        $('#assumptionsC12').val(assumptionsC12);
        $('#assumptionsD12').val(assumptionsD12);
        $('#assumptionsE12').val(assumptionsE12);
        $('#assumptionsF12').val(assumptionsF12);
        $('#assumptionsG12').val(assumptionsG12);

        // debit type radio button
        var assumptionsC13 = Number($("input[name='assumptionsC13']:checked").val());
        // Amount of debt
        var waccB3 = (initialDataC12 + evcForShareholdersB14);
        var assumptionsB27 = Number(waccB3.toFixed(3));
        $('#assumptionsB27').val(assumptionsB27.toFixed(1));
        // Minimun cash
        // T1
        // Sales
        var accountingD22 = Number(assumptionsC12);
        var accountingE22 = Number(assumptionsD12);
        var accountingF22 = Number(assumptionsE12);
        var accountingG22 = Number(assumptionsF12);
        var accountingH22 = Number(assumptionsG12);
        if (assumptionsC1 === 1) {
            accountingD22 = Number(assumptionsC11);
            accountingE22 = Number(assumptionsD11);
            accountingF22 = Number(assumptionsE11);
            accountingG22 = Number(assumptionsF11);
            accountingH22 = Number(assumptionsG11);
        }

        var assumptionsC31 = assumptionsB22 * accountingD22;
        var assumptionsD31 = assumptionsB22 * accountingE22;
        var assumptionsE31 = assumptionsB22 * accountingF22;
        var assumptionsF31 = assumptionsB22 * accountingG22;
        var assumptionsG31 = assumptionsB22 * accountingH22;

        $('#assumptionsC31').val(assumptionsC31.toFixed(1));
        $('#assumptionsD31').val(assumptionsD31.toFixed(1));
        $('#assumptionsE31').val(assumptionsE31.toFixed(1));
        $('#assumptionsF31').val(assumptionsF31.toFixed(1));
        $('#assumptionsG31').val(assumptionsG31.toFixed(1));

        // Keu
        var assumptionsB42 = assumptionsB35 + assumptionsB36 * assumptionsB37;
        // ROCE last year
        // =+FCF!G5/(Accounting!H5+Accounting!H6+Accounting!H7)
        var wcC2 = assumptionsC12;
        var wcD2 = assumptionsD12;
        var wcE2 = assumptionsE12;
        var wcF2 = assumptionsF12;
        var wcG2 = assumptionsG12;
        if (assumptionsC1 === 1) {
            wcC2 = Number(Number(assumptionsC11).toFixed(1));
            wcD2 = Number(Number(assumptionsD11).toFixed(1));
            wcE2 = Number(Number(assumptionsE11).toFixed(1));
            wcF2 = Number(Number(assumptionsF11).toFixed(1));
            wcG2 = Number(Number(assumptionsG11).toFixed(1));
        }

        $('#wcC2').val(wcC2);
        $('#wcD2').val(wcD2);
        $('#wcE2').val(wcE2);
        $('#wcF2').val(wcF2);
        $('#wcG2').val(wcG2);

        var wcC3 = -1 * assumptionsC14 * wcC2;
        var wcD3 = -1 * assumptionsD14 * wcD2;
        var wcE3 = -1 * assumptionsE14 * wcE2;
        var wcF3 = -1 * assumptionsF14 * wcF2;
        var wcG3 = -1 * assumptionsG14 * wcG2;

        var wcG6 = assumptionsG20 * wcG2 / daysOfYear;
        var accountingH2 = wcG6;
        // Inventories
        var wcG7 = Math.abs(assumptionsG19 * wcG3 / daysOfYear);

        var accountingH3 = wcG7;
        // Acc Payables
        var wcG8 = Number(assumptionsG21 * wcG3 / daysOfYear);
        var accountingH4 = wcG8;

        var accountingB7 = initialDataC9;
        var accountingC7 = accountingB7;
        var accountingD7 = accountingC7 - assumptionsC32;
        var accountingE7 = accountingD7 - assumptionsD32;
        var accountingF7 = accountingE7 - assumptionsE32;
        var accountingG7 = accountingF7 - assumptionsF32;
        // Total Operat WC
        var accountingH5 = accountingH2 + accountingH3 + accountingH4;
        var accountingH6 = assumptionsG31;
        var accountingH7 = accountingG7 - assumptionsG32;

        var accountingH23 = -1 * accountingH22 * assumptionsG14;
        // OPEX
        var accountingH24 = -1 * accountingH22 * assumptionsG15;
        // Eeprecations
        var accountingD25 = -1 * assumptionsB16;
        var accountingE25 = accountingD25 * (1 + assumptionsB17);
        var accountingF25 = accountingE25 * (1 + assumptionsB17);
        var accountingG25 = accountingF25 * (1 + assumptionsB17);
        var accountingH25 = accountingG25 * (1 + assumptionsB17);
        // EBIT
        var accountingH26 = accountingH22 + accountingH23 + accountingH24 + accountingH25;

        var fcfG3 = Number(accountingH26);
        var fcfG4 = -1 * fcfG3 * assumptionsB38;
        var fcfG5 = fcfG3 + fcfG4;

        var assumptionsB44 = fcfG5 / (accountingH5 + accountingH6 + accountingH7);

        var wcC6 = assumptionsC20 * wcC2 / daysOfYear;
        var wcC7 = Math.abs(assumptionsC19 * wcC3 / daysOfYear);
        var wcC8 = assumptionsC21 * wcC3 / daysOfYear;
        var wcC9 = Number(assumptionsC31);
        var wcC10 = wcC6 + wcC7 + wcC8 + wcC9;

        var wcD6 = assumptionsD20 * wcD2 / daysOfYear;
        var wcD7 = Math.abs(assumptionsD19 * wcD3 / daysOfYear);
        var wcD8 = assumptionsD21 * wcD3 / daysOfYear;
        var wcD9 = assumptionsD31;
        var wcD10 = wcD6 + wcD7 + wcD8 + wcD9;

        var wcD12 = wcC10 - wcD10;

        // Cost of sales
        var accountingE23 = -1 * accountingE22 * assumptionsD14;
        // OPEX
        var accountingE24 = -1 * accountingE22 * assumptionsD15;
        // EBIT
        var accountingE26 = accountingE22 + accountingE23 + accountingE24 + accountingE25;

        var fcfD3 = Number(accountingE26);

        var fcfD4 = -1 * fcfD3 * assumptionsB38;

        var fcfD5 = fcfD3 + fcfD4;
        var fcfD6 = -1 * accountingE25;
        var fcfD7 = wcD12;

        var fcfD8 = -1 * fcfD6;

        var fcfD9 = assumptionsD32;

        var fcfD10 = fcfD5 + fcfD6 + fcfD7 + fcfD8 + fcfD9;

        var waccB2 = waccB4 - waccB3;

        var waccB6 = waccB3 / waccB2;

        var waccB8 = waccB2 / waccB4;
        var waccB9 = 1 - waccB8;
        var waccB10 = assumptionsB42 + waccB6 * (assumptionsB42 - assumptionsB34);
        var waccB11 = assumptionsB34 * (1 - assumptionsB38);

        // WACC =SUMAPRODUCTO(B8:B9;B10:B11)
        var waccB12 = (waccB8 * waccB10) + (waccB9 * waccB11);

        // Cost of sales
        var accountingD23 = -1 * accountingD22 * assumptionsC14;
        // OPEX
        var accountingD24 = -1 * accountingD22 * assumptionsC15;
        // EBIT
        var accountingD26 = accountingD22 + accountingD23 + accountingD24 + accountingD25;

        var fcfC3 = Number(accountingD26);

        var fcfC4 = -1 * fcfC3 * assumptionsB38;


        var wcB6 = initialDataC4;
        var wcB7 = initialDataC5;
        var wcB8 = initialDataC6;
        var wcB9 = initialDataC7;
        var wcB10 = wcB6 + wcB7 + wcB8 + wcB9;
        var wcC12 = wcB10 - wcC10;

        var fcfC5 = fcfC3 + fcfC4;
        var fcfC6 = -1 * accountingD25;
        var fcfC7 = wcC12;
        var fcfC8 = -1 * fcfC6;
        var fcfC9 = assumptionsC32;


        var fcfC10 = fcfC5 + fcfC6 + fcfC7 + fcfC8 + fcfC9;

        var waccC4 = waccB4 * (1 + waccB12) - fcfC10;

        var waccC3 = assumptionsC29;
        if (assumptionsC13 === 1) {
            waccC3 = assumptionsC28 * waccC4;
        }

        var waccC2 = waccC4 - waccC3;
        var waccC6 = waccC3 / waccC2;

        var waccC8 = waccC2 / waccC4;
        var waccC9 = waccC3 / waccC4;
        var waccC10 = assumptionsB42 + waccC6 * (assumptionsB42 - assumptionsB34);
        var waccC11 = assumptionsB34 * (1 - assumptionsB38);
        var waccC12 = (waccC8 * waccC10) + (waccC9 * waccC11);

        var waccD4 = waccC4 * (1 + waccC12) - fcfD10;

        var waccD3 = assumptionsD29;
        if (assumptionsC13 === 1) {
            waccD3 = assumptionsD28 * waccD4;
        }
        var waccD2 = waccD4 - waccD3;
        var waccD6 = waccD3 / waccD2;

        var waccD10 = assumptionsB42 + waccD6 * (assumptionsB42 - assumptionsB34);
        // Cost of sales
        var accountingF23 = -1 * accountingF22 * assumptionsE14;
        // OPEX
        var accountingF24 = -1 * accountingF22 * assumptionsE15;
        // Eeprecations
        var accountingF26 = accountingF22 + accountingF23 + accountingF24 + accountingF25;
        var fcfE3 = Number(accountingF26);
        var fcfE4 = -1 * fcfE3 * assumptionsB38;

        var fcfE5 = fcfE3 + fcfE4;
        var wcE6 = assumptionsE20 * wcE2 / daysOfYear;
        var wcE7 = Math.abs(assumptionsE19 * wcE3 / daysOfYear);
        var wcE8 = assumptionsE21 * wcE3 / daysOfYear;
        var wcE9 = assumptionsE31;

        var wcF6 = assumptionsF20 * wcF2 / daysOfYear;
        var wcF7 = Math.abs(assumptionsF19 * wcF3 / daysOfYear);
        var wcF8 = assumptionsF21 * wcF3 / daysOfYear;
        var wcF9 = assumptionsF31;
        var wcF10 = wcF6 + wcF7 + wcF8 + wcF9;

        var wcE10 = wcE6 + wcE7 + wcE8 + wcE9;
        var wcE12 = wcD10 - wcE10;
        var fcfE7 = wcE12;
        var wcF12 = wcE10 - wcF10;

        // Cost of sales
        var accountingG23 = -1 * accountingG22 * assumptionsF14;
        // OPEX
        var accountingG24 = -1 * accountingG22 * assumptionsF15;
        // EBIT
        var accountingG26 = accountingG22 + accountingG23 + accountingG24 + accountingG25;

        var fcfF3 = Number(accountingG26);
        var fcfF4 = -1 * fcfF3 * assumptionsB38;
        var fcfF5 = fcfF3 + fcfF4;

        var fcfE9 = assumptionsE32;
        var fcfE6 = -1 * accountingF25;
        var fcfE8 = -1 * fcfE6;
        var fcfF6 = -1 * accountingG25;
        var fcfF7 = wcF12;
        var fcfF8 = -1 * fcfF6;
        var fcfF9 = assumptionsF32;

        var waccD8 = waccD2 / waccD4;
        var waccD9 = waccD3 / waccD4;
        var waccD11 = assumptionsB34 * (1 - assumptionsB38);
        var waccD12 = (waccD8 * waccD10) + (waccD9 * waccD11);
        var fcfE10 = fcfE5 + fcfE6 + fcfE7 + fcfE8 + fcfE9;
        var waccE4 = waccD4 * (1 + waccD12) - fcfE10;
        var waccE3 = assumptionsE29;
        if (assumptionsC13 === 1) {
            waccE3 = assumptionsE28 * waccE4;
        }
        var waccE2 = waccE4 - waccE3;
        var waccE6 = waccE3 / waccE2;
        var waccE8 = waccE2 / waccE4;
        var waccE10 = assumptionsB42 + waccE6 * (assumptionsB42 - assumptionsB34);
        var waccE9 = waccE3 / waccE4;
        var waccE11 = assumptionsB34 * (1 - assumptionsB38);

        var fcfF10 = fcfF5 + fcfF6 + fcfF7 + fcfF8 + fcfF9;

        var waccE12 = (waccE8 * waccE10) + (waccE9 * waccE11);


        var waccF4 = waccE4 * (1 + waccE12) - fcfF10;

        var waccF3 = assumptionsF29;
        if (assumptionsC13 === 1) {
            waccF3 = assumptionsF28 * waccF4;
        }
        var waccF2 = waccF4 - waccF3;

        var waccF6 = waccF3 / waccF2;
        var waccF8 = waccF2 / waccF4;
        var waccF9 = waccF3 / waccF4;

        var waccF10 = assumptionsB42 + waccF6 * (assumptionsB42 - assumptionsB34);
        var waccF11 = assumptionsB34 * (1 - assumptionsB38);
        var waccF12 = (waccF8 * waccF10) + (waccF9 * waccF11);
        var assumptionsB41 = Number(waccF12);

        $('#assumptionsB41').val((assumptionsB41 * 100).toFixed(1));
        $('#assumptionsB42').val((assumptionsB42 * 100).toFixed(1));
        $('#assumptionsB44').val((assumptionsB44 * 100).toFixed(1));
        /**
         * calcula WC
         */
        /** /////////////////////////
         *  WC
         */
        // Sales

        // Cost of sales
        //T1-T5
        $('#wcC3').val(wcC3.toFixed(1));
        $('#wcD3').val(wcD3.toFixed(1));
        $('#wcE3').val(wcE3.toFixed(1));
        $('#wcF3').val(wcF3.toFixed(1));
        $('#wcG3').val(wcG3.toFixed(1));

        // Pre Balance
        // Acc Receivable
        $('#wcB6').val(wcB6.toFixed(1));
        // Inventories
        $('#wcB7').val(wcB7.toFixed(1));
        // Acc Payables
        $('#wcB8').val(wcB8.toFixed(1));
        // Min Cash
        $('#wcB9').val(wcB9.toFixed(1));
        // WC
        $('#wcB10').val(wcB10.toFixed(1));

        //t1
        // Acc Receivable =Assumptions!F11*WC!C2/daysOfYear
        $('#wcC6').val(wcC6.toFixed(1));
        // Inventories =ABS(Assumptions!F10*WC!C3/365)
        $('#wcC7').val(wcC7.toFixed(1));
        // Acc Payables =Assumptions!F12*WC!C3/365
        $('#wcC8').val(wcC8.toFixed(1));
        // Min Cash =Assumptions!F17
        $('#wcC9').val(assumptionsC31.toFixed(1));
        // WC
        $('#wcC10').val(wcC10.toFixed(1));

        //t2
        // Acc Receivable =Assumptions!G11*WC!D2/daysOfYear
        $('#wcD6').val(wcD6.toFixed(1));
        // Inventories =ABS(Assumptions!G10*WC!D3/365)
        $('#wcD7').val(wcD7.toFixed(1));
        // Acc Payables =Assumptions!G12*WC!D3/365
        $('#wcD8').val(wcD8.toFixed(1));
        // Min Cash =Assumptions!G17
        $('#wcD9').val(wcD9.toFixed(1));
        // WC
        $('#wcD10').val(wcD10.toFixed(1));

        //t3
        // Acc Receivable =Assumptions!GH11*WC!E2/daysOfYear
        $('#wcE6').val(wcE6.toFixed(1));
        // Inventories =ABS(Assumptions!H10*WC!E3/365)
        $('#wcE7').val(wcE7.toFixed(1));
        // Acc Payables =Assumptions!H12*WC!E3/365
        $('#wcE8').val(wcE8.toFixed(1));
        // Min Cash =Assumptions!H17
        $('#wcE9').val(wcE9.toFixed(1));
        // WC
        $('#wcE10').val(wcE10.toFixed(1));

        //t4
        // Acc Receivable =Assumptions!I11*WC!F2/daysOfYear
        $('#wcF6').val(wcF6.toFixed(1));
        // Inventories =ABS(Assumptions!I10*WC!F3/365)
        $('#wcF7').val(wcF7.toFixed(1));
        // Acc Payables =Assumptions!I12*WC!F3/365
        $('#wcF8').val(wcF8.toFixed(1));
        // Min Cash =Assumptions!I17
        $('#wcF9').val(wcF9.toFixed(1));
        // WC
        $('#wcF10').val(wcF10.toFixed(1));

        //t5
        var wcG9 = assumptionsG31;
        var wcG10 = wcG6 + wcG7 + wcG8 + wcG9;
        // Acc Receivable =Assumptions!J11*WC!G2/daysOfYear
        $('#wcG6').val(wcG6.toFixed(1));
        // Inventories =ABS(Assumptions!J10*WC!G3/365)
        $('#wcG7').val(wcG7.toFixed(1));
        // Acc Payables =Assumptions!J12*WC!G3/365
        $('#wcG8').val(wcG8.toFixed(1));
        // Min Cash =Assumptions!J17
        $('#wcG9').val(wcG9.toFixed(1));
        // WC
        $('#wcG10').val(wcG10.toFixed(1));

        // Var WC
        var wcG12 = wcF10 - wcG10;
        // T1-T5
        $('#wcC12').val(wcC12.toFixed(1));
        $('#wcD12').val(wcD12.toFixed(1));
        $('#wcE12').val(wcE12.toFixed(1));
        $('#wcF12').val(wcF12.toFixed(1));
        $('#wcG12').val(wcG12.toFixed(1));

        /**
         * calcula FFC
         */
        // t1
        $('#fcfC3').val(Number(fcfC3).toFixed(1));
        // t2
        $('#fcfD3').val(fcfD3.toFixed(1));
        // t3
        $('#fcfE3').val(fcfE3.toFixed(1));
        // t4
        $('#fcfF3').val(fcfF3.toFixed(1));
        // t5
        $('#fcfG3').val(fcfG3.toFixed(1));

        // t1
        $('#fcfC4').val(fcfC4.toFixed(1));
        // t2
        $('#fcfD4').val(fcfD4.toFixed(1));
        // t3
        $('#fcfE4').val(fcfE4.toFixed(1));
        // t4
        $('#fcfF4').val(fcfF4.toFixed(1));
        // t5
        $('#fcfG4').val(fcfG4.toFixed(1));

        // EBIaT
        // t1
        $('#fcfC5').val(fcfC5.toFixed(1));
        // t2
        $('#fcfD5').val(fcfD5.toFixed(1));
        // t3
        $('#fcfE5').val(fcfE5.toFixed(1));
        // t4
        $('#fcfF5').val(fcfF5.toFixed(1));
        // t5
        $('#fcfG5').val(fcfG5.toFixed(1));

        // Depreciation
        // t1
        $('#fcfC6').val(fcfC6.toFixed(1));
        // t2
        $('#fcfD6').val(fcfD6.toFixed(1));
        // t3
        $('#fcfE6').val(fcfE6.toFixed(1));
        // t4
        $('#fcfF6').val(fcfF6.toFixed(1));
        // t5
        var fcfG6 = -1 * accountingH25;
        $('#fcfG6').val(fcfG6.toFixed(1));

        // Var Working Cap
        // t1
        $('#fcfC7').val(fcfC7.toFixed(1));
        // t2
        $('#fcfD7').val(fcfD7.toFixed(1));
        // t3
        $('#fcfE7').val(fcfE7.toFixed(1));
        // t4
        $('#fcfF7').val(fcfF7.toFixed(1));
        // t5
        var fcfG7 = wcG12;
        $('#fcfG7').val(fcfG7.toFixed(1));

        // CAPEX Maintenance
        // t1
        $('#fcfC8').val(fcfC8.toFixed(1));
        // t2
        $('#fcfD8').val(fcfD8.toFixed(1));
        // t3
        $('#fcfE8').val(fcfE8.toFixed(1));
        // t4
        $('#fcfF8').val(fcfF8.toFixed(1));
        // t5
        var fcfG8 = -1 * fcfG6;
        $('#fcfG8').val(fcfG8.toFixed(1));

        // CAPEX new investment
        // t1
        $('#fcfC9').val(fcfC9.toFixed(1));
        // t2
        $('#fcfD9').val(fcfD9.toFixed(1));
        // t3
        $('#fcfE9').val(fcfE9.toFixed(1));
        // t4
        $('#fcfF9').val(fcfF9.toFixed(1));
        // t5
        var fcfG9 = assumptionsG32;
        $('#fcfG9').val(fcfG9.toFixed(1));

        // FCF
        // t1
        $('#fcfC10').val(fcfC10.toFixed(1));
        // t2
        $('#fcfD10').val(fcfD10.toFixed(1));
        // t3
        $('#fcfE10').val(fcfE10.toFixed(1));
        // t4
        $('#fcfF10').val(fcfF10.toFixed(1));
        // t5
        var fcfG10 = Number(fcfG5 + fcfG6 + fcfG7 + fcfG8 + fcfG9);
        $('#fcfG10').val(fcfG10.toFixed(1));

        // EBIaT to extrapolate
        // =Assumptions!B26*(Accounting!H7+Accounting!H5)
        var fcfB12 = assumptionsB40 * (accountingH7 + accountingH5);
        $('#fcfB12').val(fcfB12.toFixed(1));
        // Average CAPEX
        // =PROMEDIO(C9:G9)
        var fcfB13 = (fcfC9 + fcfD9 + fcfE9 + fcfF9 + fcfG9) / 5;
        $('#fcfB13').val(fcfB13.toFixed(1));
        // FCF to extrapolate
        // =B12+SUMA(G6:G8)+B13
        var fcfB14 = fcfB12 + fcfB13 + (fcfG6 + fcfG7 + fcfG8);
        $('#fcfB14').val(fcfB14.toFixed(1));
        // TV
        // =B14*(1+Assumptions!B25)/(WACC!G12-Assumptions!B25)
        var fcfB15 = fcfB14 * (1 + assumptionsB39) / (waccF12 - assumptionsB39);
        $('#fcfB15').val(fcfB15.toFixed(1));

        // FCF + TV
        // t1
        var fcfC17 = fcfC10;
        $('#fcfC17').val(fcfC17.toFixed(1));
        // t2
        var fcfD17 = fcfD10;
        $('#fcfD17').val(fcfD17.toFixed(1));
        // t3
        var fcfE17 = fcfE10;
        $('#fcfE17').val(fcfE17.toFixed(1));
        // t4
        var fcfF17 = fcfF10;
        $('#fcfF17').val(fcfF17.toFixed(1));
        // t5
        var fcfG17 = fcfG10 + fcfB15;
        $('#fcfG17').val(fcfG17.toFixed(1));

        // VE
        // =SUMAPRODUCTO(C17:G17;WACC!C13:G13)
        var waccB13 = 1 / (1 + waccB12);
        var waccC13 = waccB13 / (1 + waccC12);
        var waccD13 = waccC13 / (1 + waccD12);
        var waccE13 = waccD13 / (1 + waccE12);
        var waccF13 = waccE13 / (1 + waccF12);

        fcfB19 = (fcfC17 * waccB13) + (fcfD17 * waccC13) + (fcfE17 * waccD13) + (fcfF17 * waccE13) + (fcfG17 * waccF13);

        $('#fcfB19').val(fcfB19.toFixed(1));

        // Debt
        var fcfB20 = initialDataC12;
        $('#fcfB20').val(fcfB20.toFixed(1));

        // Equity
        fcfB21 = fcfB19 - fcfB20;
        $('#fcfB21').val(fcfB21.toFixed(1));

        // Veu
        // =VNA(Assumptions!B28;FCF!C17:G17)
        var fcfB25 = (fcfC17 / (1 + assumptionsB42)) +
            (fcfD17 / Math.pow((1 + assumptionsB42), 2)) +
            (fcfE17 / Math.pow((1 + assumptionsB42), 3)) +
            (fcfF17 / Math.pow((1 + assumptionsB42), 4)) +
            (fcfG17 / Math.pow((1 + assumptionsB42), 5));
        $('#fcfB25').val(fcfB25.toFixed(1));

        var accountingB11 = initialDataC12;
        var accountingC11 = accountingB11 + evcForShareholdersB14;
        var accountingD11 = waccC3;
        var accountingE11 = waccD3;
        var accountingF11 = waccE3;
        var accountingG11 = waccF3;

        var accountingD27 = -1 * assumptionsB34 * accountingC11;
        var accountingE27 = -1 * assumptionsB34 * accountingD11;
        var accountingF27 = -1 * assumptionsB34 * accountingE11;
        var accountingG27 = -1 * assumptionsB34 * accountingF11;
        var accountingH27 = -1 * assumptionsB34 * accountingG11;

        var evcForShareholdersC4 = accountingD27 * (1 - assumptionsB38);
        var evcForShareholdersD4 = accountingE27 * (1 - assumptionsB38);
        var evcForShareholdersE4 = accountingF27 * (1 - assumptionsB38);
        var evcForShareholdersF4 = accountingG27 * (1 - assumptionsB38);
        var evcForShareholdersG4 = accountingH27 * (1 - assumptionsB38);

        var fcfB26 = -1 * ((evcForShareholdersC4 / (1 + assumptionsB42)) +
            (evcForShareholdersD4 / Math.pow((1 + assumptionsB42), 2)) +
            (evcForShareholdersE4 / Math.pow((1 + assumptionsB42), 3)) +
            (evcForShareholdersF4 / Math.pow((1 + assumptionsB42), 4)) +
            (evcForShareholdersG4 / Math.pow((1 + assumptionsB42), 5))) *
            assumptionsB38 / (1 - assumptionsB38);
        $('#fcfB26').val(fcfB26.toFixed(1));

        // Veu + VA(AF)
        var fcfB27 = fcfB25 + fcfB26;
        $('#fcfB27').val(fcfB27.toFixed(1));

        // TV without adjustment in ROCE
        // =+(B14+(G5-B12))*(1+Assumptions!B39)/(WACC!F12-Assumptions!B39)
        var fcfC32 = (fcfB14 + (fcfG5 - fcfB12)) * (1 + assumptionsB39) / (waccF12 - assumptionsB39);
        $('#fcfC32').val(fcfC32.toFixed(1));

        /**
         * calcula WACC
         */
        // T
        // Debt (D)
        $('#waccB3').val(waccB3.toFixed(1));
        $('#waccA3').val(initialDataC12.toFixed(1));

        // Equity ( E)
        var waccA2 = fcfB21;
        $('#waccB2').val(waccB2.toFixed(1));
        $('#waccA2').val(waccA2.toFixed(1));
        //d+e
        $('#waccA4').val(initialDataC14.toFixed(1));

        // var waccA6 = waccA3 / waccA2;
        var waccA6 = initialDataC12 / waccA2;
        $('#waccA6').val(waccA6.toFixed(3));

        // =FCF!B19-WACC!B4
        var waccB5 = fcfB19 - waccB4;
        $('#waccB5').val(waccB5.toFixed(3));

        // Financial Leverage (L)
        $('#waccB6').val(waccB6.toFixed(3));
        // Percetage of Equity (We)
        $('#waccB8').val((waccB8 * 100).toFixed(2));
        // Percentage of Debt (Wd)
        $('#waccB9').val((waccB9 * 100).toFixed(2));
        // Cost of Equity Leverage (Kel) =Assumptions!$B$42+WACC!B6*(Assumptions!$B$42-Assumptions!$B$34)
        $('#waccB10').val((waccB10 * 100).toFixed(2));
        // Cost of Debt (Kd)
        $('#waccB11').val((waccB11 * 100).toFixed(2));
        // WACC =SUMAPRODUCTO(B8:B9;B10:B11)
        $('#waccB12').val((waccB12 * 100).toFixed(2));

        // T+1
        // Equity ( E)
        $('#waccC2').val(waccC2.toFixed(1));
        $('#waccC3').val(waccC3.toFixed(1));
        // Enterprise Value (EV)
        $('#waccC4').val(waccC4.toFixed(1));
        // Financial Leverage (L)
        $('#waccC6').val(waccC6.toFixed(3));
        // Percetage of Equity (We)
        $('#waccC8').val((waccC8 * 100).toFixed(2));
        // Percentage of Debt (Wd)
        $('#waccC9').val((waccC9 * 100).toFixed(2));
        // Cost of Equity Leverage (Kel)
        // =Assumptions!$B$42+WACC!C6*(Assumptions!$B$42-Assumptions!$B$34)
        $('#waccC10').val((waccC10 * 100).toFixed(2));
        // Cost of Debt (Kd) =Assumptions!$B$34*(1-Assumptions!$B$38)
        $('#waccC11').val((waccC11 * 100).toFixed(2));
        // WACC =SUMAPRODUCTO(C8:C9;C10:C11)
        $('#waccC12').val((waccC12 * 100).toFixed(2));

        // T+2
        // Enterprise Value (EV)
        $('#waccD4').val(waccD4.toFixed(1));
        $('#waccD3').val(waccD3.toFixed(1));
        // Equity ( E)
        $('#waccD2').val(waccD2.toFixed(1));
        // Financial Leverage (L)
        $('#waccD6').val(waccD6.toFixed(3));
        // Percetage of Equity (We)
        $('#waccD8').val((waccD8 * 100).toFixed(2));
        // Percentage of Debt (Wd)
        $('#waccD9').val((waccD9 * 100).toFixed(2));
        // Cost of Equity Leverage (Kel)
        // =Assumptions!$B$42+WACC!C6*(Assumptions!$B$42-Assumptions!$B$34)
        $('#waccD10').val((waccD10 * 100).toFixed(2));
        // Cost of Debt (Kd) =Assumptions!$B$34*(1-Assumptions!$B$38)
        $('#waccD11').val((waccD11 * 100).toFixed(2));
        // WACC =SUMAPRODUCTO(D8:D9;D10:D11)
        $('#waccD12').val((waccD12 * 100).toFixed(2));
        // T+3
        // Enterprise Value (EV)
        $('#waccE4').val(waccE4.toFixed(1));
        $('#waccE3').val(waccE3.toFixed(1));
        // Equity ( E)
        $('#waccE2').val(waccE2.toFixed(1));
        // Financial Leverage (L)
        $('#waccE6').val(waccE6.toFixed(3));
        // Percetage of Equity (We)
        $('#waccE8').val((waccE8 * 100).toFixed(2));
        // Percentage of Debt (Wd)
        $('#waccE9').val((waccE9 * 100).toFixed(2));
        // Cost of Equity Leverage (Kel)
        // =Assumptions!$B$42+WACC!E6*(Assumptions!$B$42-Assumptions!$B$34)
        $('#waccE10').val((waccE10 * 100).toFixed(2));
        // Cost of Debt (Kd) =Assumptions!$B$34*(1-Assumptions!$B$38)
        $('#waccE11').val((waccE11 * 100).toFixed(2));
        // WACC =SUMAPRODUCTO(D8:D9;D10:D11)
        $('#waccE12').val((waccE12 * 100).toFixed(2));

        // T+4
        // Enterprise Value (EV)
        $('#waccF4').val(waccF4.toFixed(1));
        $('#waccF3').val(waccF3.toFixed(1));
        // Equity ( E)
        $('#waccF2').val(waccF2.toFixed(1));
        // Financial Leverage (L)
        $('#waccF6').val(waccF6.toFixed(3));
        // Percetage of Equity (We)
        $('#waccF8').val((waccF8 * 100).toFixed(2));
        // Percentage of Debt (Wd)
        $('#waccF9').val((waccF9 * 100).toFixed(2));
        // Cost of Equity Leverage (Kel)
        // =Assumptions!$B$42+WACC!E6*(Assumptions!$B$42-Assumptions!$B$34)
        $('#waccF10').val((waccF10 * 100).toFixed(2));
        // Cost of Debt (Kd) =Assumptions!$B$34*(1-Assumptions!$B$38)
        $('#waccF11').val((waccF11 * 100).toFixed(2));
        // WACC =SUMAPRODUCTO(D8:D9;D10:D11)
        $('#waccF12').val((waccF12 * 100).toFixed(2));

        // T+5
        // Enterprise Value (EV)
        var waccG4 = waccF4 * (1 + waccF12) - fcfG10;
        var waccG3 = assumptionsG29;
        if (assumptionsC13 === 1) {
            waccG3 = assumptionsG28 * waccG4;
        }
        $('#waccG4').val(waccG4.toFixed(1));
        $('#waccG3').val(waccG3.toFixed(1));
        // Equity ( E)
        var waccG2 = waccG4 - waccG3;
        $('#waccG2').val(waccG2.toFixed(1));
        // Financial Leverage (L)
        var waccG6 = waccG3 / waccG2;
        $('#waccG6').val(waccG6.toFixed(3));

        // Capital Structure Evolution
        // At EV
        // T
        // Equity ( E)
        var waccB25 = waccB2 / waccB4;
        $('#waccB25').val((waccB25 * 100).toFixed(1));
        // Debt (D)
        var waccB26 = waccB3 / waccB4;
        $('#waccB26').val((waccB26 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccB27 = waccB25 + waccB26;
        $('#waccB27').val((waccB27 * 100).toFixed(1));

        // T1
        // Equity ( E)
        var waccC25 = waccC2 / waccC4;
        $('#waccC25').val((waccC25 * 100).toFixed(1));
        // Debt (D)
        var waccC26 = waccC3 / waccC4;
        $('#waccC26').val((waccC26 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccC27 = waccC25 + waccC26;
        $('#waccC27').val((waccC27 * 100).toFixed(1));

        // T2
        // Equity ( E)
        var waccD25 = waccD2 / waccD4;
        $('#waccD25').val((waccD25 * 100).toFixed(1));
        // Debt (D)
        var waccD26 = waccD3 / waccD4;
        $('#waccD26').val((waccD26 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccD27 = waccD25 + waccD26;
        $('#waccD27').val((waccD27 * 100).toFixed(1));

        // T3
        // Equity ( E)
        var waccE25 = waccE2 / waccE4;
        $('#waccE25').val((waccE25 * 100).toFixed(1));
        // Debt (D)
        var waccE26 = waccE3 / waccE4;
        $('#waccE26').val((waccE26 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccE27 = waccE25 + waccE26;
        $('#waccE27').val((waccE27 * 100).toFixed(1));

        // T4
        // Equity ( E)
        var waccF25 = waccF2 / waccF4;
        $('#waccF25').val((waccF25 * 100).toFixed(1));
        // Debt (D)
        var waccF26 = waccF3 / waccF4;
        $('#waccF26').val((waccF26 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccF27 = waccF25 + waccF26;
        $('#waccF27').val((waccF27 * 100).toFixed(1));

        // T5
        // Equity ( E)
        var waccG25 = waccG2 / waccG4;
        $('#waccG25').val((waccG25 * 100).toFixed(1));
        // Debt (D)
        var waccG26 = waccG3 / waccG4;
        $('#waccG26').val((waccG26 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccG27 = waccG25 + waccG26;
        $('#waccG27').val((waccG27 * 100).toFixed(1));

        // At EV
        // T
        // Equity ( E)

        var evcForShareholdersB15 = (-1 * evcForShareholdersB13) - evcForShareholdersB14;

        var accountingC12 = evcForShareholdersB15;
        var accountingC13 = accountingC11 + accountingC12;

        var waccB32 = accountingC12 / accountingC13;
        $('#waccB32').val((waccB32 * 100).toFixed(1));
        // Debt (D)
        var waccB33 = accountingC11 / accountingC13;
        $('#waccB33').val((waccB33 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccB34 = waccB32 + waccB33;
        $('#waccB34').val((waccB34 * 100).toFixed(1));

        var accountingB12 = initialDataC13;

        var accountingC8 = (-1 * evcForShareholdersB13) - accountingB12;

        // T1
        // Acc Receivable
        var accountingD2 = wcC6;
        // Inventories
        var accountingD3 = wcC7;
        // Acc Payables
        var accountingD4 = wcC8;
        // Total Operat WC
        var accountingD5 = accountingD2 + accountingD3 + accountingD4;
        // Cash
        var accountingD6 = assumptionsC31;
        // Net Fixed Assets
        // Goodwill =-EVCforShareholders!B13-B12
        var accountingD8 = accountingC8;
        // Total Net Assets
        var accountingD9 = accountingD5 + accountingD6 + accountingD7 + accountingD8;

        var accountingD12 = accountingD9 - accountingD11;
        var accountingD13 = accountingD11 + accountingD12;

        var waccC32 = accountingD12 / accountingD13;
        $('#waccC32').val((waccC32 * 100).toFixed(1));
        // Debt (D)
        var waccC33 = accountingD11 / accountingD13;
        $('#waccC33').val((waccC33 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccC34 = waccC32 + waccC33;
        $('#waccC34').val((waccC34 * 100).toFixed(1));

        // T2
        // Acc Receivable
        var accountingE2 = wcD6;
        // Inventories
        var accountingE3 = wcD7;
        // Acc Payables
        var accountingE4 = wcD8;
        // Total Operat WC
        var accountingE5 = accountingE2 + accountingE3 + accountingE4;
        // Cash
        var accountingE6 = assumptionsD31;
        // Net Fixed Assets
        var accountingE8 = accountingD8;
        // Total Net Assets
        var accountingE9 = accountingE5 + accountingE6 + accountingE7 + accountingE8;

        var accountingE12 = accountingE9 - accountingE11;
        var accountingE13 = accountingE11 + accountingE12;

        // T1
        // Equity ( E)
        var waccD32 = accountingE12 / accountingE13;
        $('#waccD32').val((waccD32 * 100).toFixed(1));
        // Debt (D)
        var waccD33 = accountingE11 / accountingE13;
        $('#waccD33').val((waccD33 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccD34 = waccD32 + waccD33;
        $('#waccD34').val((waccD34 * 100).toFixed(1));

        // T3
        // Acc Receivable
        var accountingF2 = wcE6;
        // Inventories
        var accountingF3 = wcE7;
        // Acc Payables
        var accountingF4 = wcE8;
        // Total Operat WC
        var accountingF5 = accountingF2 + accountingF3 + accountingF4;
        // Cash
        var accountingF6 = assumptionsE31;
        // Net Fixed Assets
        var accountingF8 = accountingE8;
        // Total Net Assets
        var accountingF9 = accountingF5 + accountingF6 + accountingF7 + accountingF8;

        // Debit
        var accountingF12 = accountingF9 - accountingF11;
        var accountingF13 = accountingF11 + accountingF12;

        // T2
        // Equity ( E)
        var waccE32 = accountingF12 / accountingF13;
        $('#waccE32').val((waccE32 * 100).toFixed(1));
        // Debt (D)
        var waccE33 = accountingF11 / accountingF13;
        $('#waccE33').val((waccE33 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccE34 = waccE32 + waccE33;
        $('#waccE34').val((waccE34 * 100).toFixed(1));

        // T4
        // Acc Receivable
        var accountingG2 = wcF6;
        // Inventories
        var accountingG3 = wcF7;
        // Acc Payables
        var accountingG4 = wcF8;
        // Total Operat WC
        var accountingG5 = accountingG2 + accountingG3 + accountingG4;
        // Cash
        var accountingG6 = assumptionsF31;
        // Net Fixed Assets
        // Goodwill =-FVCforShareholders!B13-B12
        var accountingG8 = accountingF8;
        // Total Net Assets
        var accountingG9 = accountingG5 + accountingG6 + accountingG7 + accountingG8;

        // Debit
        var accountingG12 = accountingG9 - accountingG11;
        var accountingG13 = accountingG11 + accountingG12;

        // T3
        // Equity ( E)
        var waccF32 = accountingG12 / accountingG13;
        // Debt (D)
        var waccF33 = accountingG11 / accountingG13;
        // Enterprise Value (EV)
        var waccF34 = waccF32 + waccF33;

        // T5
        var accountingH8 = Number(accountingG8);
        // Total Net Assets
        var accountingH9 = accountingH5 + accountingH6 + accountingH7 + accountingH8;

        // Debit
        var accountingH11 = waccG3;
        var accountingH12 = accountingH9 - accountingH11;
        var accountingH13 = accountingH11 + accountingH12;

        // T4
        // Equity ( E)
        $('#waccF32').val((waccF32 * 100).toFixed(1));
        // Debt (D)
        $('#waccF33').val((waccF33 * 100).toFixed(1));
        // Enterprise Value (EV)
        $('#waccF34').val((waccF34 * 100).toFixed(1));

        // T5
        // Equity ( E)
        var waccG32 = accountingH12 / accountingH13;
        $('#waccG32').val((waccG32 * 100).toFixed(1));
        // Debt (D)
        var waccG33 = accountingH11 / accountingH13;
        $('#waccG33').val((waccG33 * 100).toFixed(1));
        // Enterprise Value (EV)
        var waccG34 = waccG32 + waccG33;
        $('#waccG34').val((waccG34 * 100).toFixed(1));

        /**
         * calcula EVC for Shareholders
         */
            // Equity Value by FCF to Shareholders
            // T1
        var waccB14 = 1 / (1 + waccB10);
        var waccC14 = waccB14 / (1 + waccC10);
        var waccD14 = waccC14 / (1 + waccD10);
        var waccE14 = waccD14 / (1 + waccE10);
        var waccF14 = waccE14 / (1 + waccF10);
        // FCF
        var evcForShareholdersC3 = fcfC10;
        // Financial Expenses x ( 1 - t) =Accounting!D27*(1-Assumptions!$B$38)
        // Evolution of Debt Principal =Accounting!D11-Accounting!C11
        var evcForShareholdersC5 = accountingD11 - accountingC11;

        $('#evcForShareholdersC3').val(evcForShareholdersC3.toFixed(1));
        $('#evcForShareholdersC4').val(evcForShareholdersC4.toFixed(1));
        $('#evcForShareholdersC5').val(evcForShareholdersC5.toFixed(1));

        // T2
        // FCF
        var evcForShareholdersD3 = fcfD10;
        // Financial Expenses x ( 1 - t) =Accounting!D27*(1-Assumptions!$B$38)
        // Evolution of Debt Principal =Accounting!D11-Accounting!C11
        var evcForShareholdersD5 = accountingE11 - accountingD11;

        $('#evcForShareholdersD3').val(evcForShareholdersD3.toFixed(1));
        $('#evcForShareholdersD4').val(evcForShareholdersD4.toFixed(1));
        $('#evcForShareholdersD5').val(evcForShareholdersD5.toFixed(1));

        // T3
        // FCF
        var evcForShareholdersE3 = fcfE10;
        // Financial Expenses x ( 1 - t) =Accounting!D27*(1-Assumptions!$B$38)
        // Evolution of Debt Principal =Accounting!D11-Accounting!C11
        var evcForShareholdersE5 = accountingF11 - accountingE11;

        $('#evcForShareholdersE3').val(evcForShareholdersE3.toFixed(1));
        $('#evcForShareholdersE4').val(evcForShareholdersE4.toFixed(1));
        $('#evcForShareholdersE5').val(evcForShareholdersE5.toFixed(1));

        // T4
        // FCF
        var evcForShareholdersF3 = fcfF10;
        // Financial Expenses x ( 1 - t) =Accounting!D27*(1-Assumptions!$B$38)
        // Evolution of Debt Principal =Accounting!D11-Accounting!C11
        var evcForShareholdersF5 = accountingG11 - accountingF11;

        $('#evcForShareholdersF3').val(evcForShareholdersF3.toFixed(1));
        $('#evcForShareholdersF4').val(evcForShareholdersF4.toFixed(1));
        $('#evcForShareholdersF5').val(evcForShareholdersF5.toFixed(1));

        // T5
        // FCF
        var evcForShareholdersG3 = fcfG10;
        // Financial Expenses x ( 1 - t) =Accounting!D27*(1-Assumptions!$B$38)
        // Evolution of Debt Principal =Accounting!D11-Accounting!C11
        var evcForShareholdersG5 = accountingH11 - accountingG11;
        var evcForShareholdersG6 = waccG2;

        $('#evcForShareholdersG3').val(evcForShareholdersG3.toFixed(1));
        $('#evcForShareholdersG4').val(evcForShareholdersG4.toFixed(1));
        $('#evcForShareholdersG5').val(evcForShareholdersG5.toFixed(1));
        $('#evcForShareholdersG6').val(evcForShareholdersG6.toFixed(1));

        // FCF to Shareholders
        // T1-T5
        var evcForShareholdersC7 = evcForShareholdersC3 + evcForShareholdersC4 + evcForShareholdersC5;
        var evcForShareholdersD7 = evcForShareholdersD3 + evcForShareholdersD4 + evcForShareholdersD5;
        var evcForShareholdersE7 = evcForShareholdersE3 + evcForShareholdersE4 + evcForShareholdersE5;
        var evcForShareholdersF7 = evcForShareholdersF3 + evcForShareholdersF4 + evcForShareholdersF5;
        var evcForShareholdersG7 = evcForShareholdersG3 + evcForShareholdersG4 + evcForShareholdersG5 + evcForShareholdersG6;

        $('#evcForShareholdersC7').val(evcForShareholdersC7.toFixed(1));
        $('#evcForShareholdersD7').val(evcForShareholdersD7.toFixed(1));
        $('#evcForShareholdersE7').val(evcForShareholdersE7.toFixed(1));
        $('#evcForShareholdersF7').val(evcForShareholdersF7.toFixed(1));
        $('#evcForShareholdersG7').val(evcForShareholdersG7.toFixed(1));

        // Equity =SUMAPRODUCTO(C7:G7;WACC!B14:F14)
        var evcForShareholdersB9 = (evcForShareholdersC7 * waccB14) +
            (evcForShareholdersD7 * waccC14) + (evcForShareholdersE7 * waccD14) +
            (evcForShareholdersF7 * waccE14) + (evcForShareholdersG7 * waccF14);
        $('#evcForShareholdersB9').val(evcForShareholdersB9.toFixed(1));

        // Equity resta =
        $('#evcForShareholdersB15').val(evcForShareholdersB15.toFixed(1));

        // If an investor
        // T-T5
        var evcForShareholdersB20 = -1 * evcForShareholdersB15;
        var evcForShareholdersC20 = evcForShareholdersC7;
        var evcForShareholdersD20 = evcForShareholdersD7;
        var evcForShareholdersE20 = evcForShareholdersE7;
        var evcForShareholdersF20 = evcForShareholdersF7;
        var evcForShareholdersG20 = evcForShareholdersG7;

        $('#evcForShareholdersB20').val(evcForShareholdersB20.toFixed(1));
        $('#evcForShareholdersC20').val(evcForShareholdersC20.toFixed(1));
        $('#evcForShareholdersD20').val(evcForShareholdersD20.toFixed(1));
        $('#evcForShareholdersE20').val(evcForShareholdersE20.toFixed(1));
        $('#evcForShareholdersF20').val(evcForShareholdersF20.toFixed(1));
        $('#evcForShareholdersG20').val(evcForShareholdersG20.toFixed(1));

        var evcForShareholdersB21;
        var K = assumptionsB42;
        if (evcForShareholdersC20 < 0 || evcForShareholdersD20 < 0 || evcForShareholdersE20 < 0
            || evcForShareholdersF20 < 0 || evcForShareholdersG20  < 0) {
            $('.alert-dismissible').addClass('show').show();
            getTirModificate(evcForShareholdersB20, evcForShareholdersC20,
                evcForShareholdersD20, evcForShareholdersE20,
                evcForShareholdersF20, evcForShareholdersG20, K).then(function (tirm) {
                evcForShareholdersB21 = tirm;
                $('#evcForShareholdersB21').val((evcForShareholdersB21 * 100).toFixed(2));
            });
        } else {
            $('.alert-dismissible').removeClass('show').hide();
            getTir(evcForShareholdersB20, evcForShareholdersC20,
                evcForShareholdersD20, evcForShareholdersE20,
                evcForShareholdersF20, evcForShareholdersG20).then(function (tir) {
                evcForShareholdersB21 = tir;
                $('#evcForShareholdersB21').val((evcForShareholdersB21 * 100).toFixed(2));
            });
        }

        // T1
        var accountingD28 = accountingD26 + accountingD27;
        var accountingD29 = -1 * Math.max(0, assumptionsB38 * accountingD28);
        var accountingD30 = accountingD28 + accountingD29;

        var accountingD39 = accountingD30 / accountingD9;
        var accountingD40 = accountingD30 / accountingD12;

        /**
         * calcula FACC
         */

        var accountingE28 = accountingE26 + accountingE27;
        var accountingE29 = -1 * Math.max(0, assumptionsB38 * accountingE28);// Net Earning
        var accountingE30 = accountingE28 + accountingE29;

        var accountingF28 = accountingF26 + accountingF27;
        var accountingF29 = -1 * Math.max(0, assumptionsB38 * accountingF28);// Net Earning
        var accountingF30 = accountingF28 + accountingF29;

        var accountingG28 = accountingG26 + accountingG27;
        var accountingG29 = -1 * Math.max(0, assumptionsB38 * accountingG28);
        var accountingG30 = accountingG28 + accountingG29;

        var accountingH28 = accountingH26 + accountingH27;
        var accountingH29 = -1 * Math.max(0, assumptionsB38 * accountingH28);
        var accountingH30 = accountingH28 + accountingH29;


        var ccfC3 = Number(accountingD30);
        var ccfD3 = Number(accountingE30);
        var ccfE3 = Number(accountingF30);
        var ccfF3 = Number(accountingG30);
        var ccfG3 = Number(accountingH30);
        var ccfC4 = -1 * accountingD25;
        var ccfD4 = -1 * accountingE25;
        var ccfE4 = -1 * accountingF25;
        var ccfF4 = -1 * accountingG25;
        var ccfG4 = -1 * accountingH25;

        $('#ccfC3').val(ccfC3.toFixed(1));
        $('#ccfD3').val(ccfD3.toFixed(1));
        $('#ccfE3').val(ccfE3.toFixed(1));
        $('#ccfF3').val(ccfF3.toFixed(1));
        $('#ccfG3').val(ccfG3.toFixed(1));

        $('#ccfC4').val(ccfC4.toFixed(1));
        $('#ccfD4').val(ccfD4.toFixed(1));
        $('#ccfE4').val(ccfE4.toFixed(1));
        $('#ccfF4').val(ccfF4.toFixed(1));
        $('#ccfG4').val(ccfG4.toFixed(1));

        var ccfC5 = ccfC3 + ccfC4;
        var ccfD5 = ccfD3 + ccfD4;
        var ccfE5 = ccfE3 + ccfE4;
        var ccfF5 = ccfF3 + ccfF4;
        var ccfG5 = ccfG3 + ccfG4;

        $('#ccfC5').val(ccfC5.toFixed(1));
        $('#ccfD5').val(ccfD5.toFixed(1));
        $('#ccfE5').val(ccfE5.toFixed(1));
        $('#ccfF5').val(ccfF5.toFixed(1));
        $('#ccfG5').val(ccfG5.toFixed(1));

        var ccfC6 = -1 * accountingD27;
        var ccfD6 = -1 * accountingE27;
        var ccfE6 = -1 * accountingF27;
        var ccfF6 = -1 * accountingG27;
        var ccfG6 = -1 * accountingH27;

        var ccfC7 = fcfC7;
        var ccfD7 = fcfD7;
        var ccfE7 = fcfE7;
        var ccfF7 = fcfF7;
        var ccfG7 = fcfG7;

        var ccfC8 = fcfC8;
        var ccfD8 = fcfD8;
        var ccfE8 = fcfE8;
        var ccfF8 = fcfF8;
        var ccfG8 = fcfG8;

        var ccfC9 = fcfC9;
        var ccfD9 = fcfD9;
        var ccfE9 = fcfE9;
        var ccfF9 = fcfF9;
        var ccfG9 = fcfG9;

        $('#ccfC6').val(ccfC6.toFixed(1));
        $('#ccfD6').val(ccfD6.toFixed(1));
        $('#ccfE6').val(ccfE6.toFixed(1));
        $('#ccfF6').val(ccfF6.toFixed(1));
        $('#ccfG6').val(ccfG6.toFixed(1));

        $('#ccfC7').val(ccfC7.toFixed(1));
        $('#ccfD7').val(ccfD7.toFixed(1));
        $('#ccfE7').val(ccfE7.toFixed(1));
        $('#ccfF7').val(ccfF7.toFixed(1));
        $('#ccfG7').val(ccfG7.toFixed(1));

        $('#ccfC8').val(ccfC8.toFixed(1));
        $('#ccfD8').val(ccfD8.toFixed(1));
        $('#ccfE8').val(ccfE8.toFixed(1));
        $('#ccfF8').val(ccfF8.toFixed(1));
        $('#ccfG8').val(ccfG8.toFixed(1));

        $('#ccfC9').val(ccfC9.toFixed(1));
        $('#ccfD9').val(ccfD9.toFixed(1));
        $('#ccfE9').val(ccfE9.toFixed(1));
        $('#ccfF9').val(ccfF9.toFixed(1));
        $('#ccfG9').val(ccfG9.toFixed(1));

        var ccfC10 = ccfC5 + ccfC6 + ccfC7 + ccfC8 + ccfC9;
        var ccfD10 = ccfD5 + ccfD6 + ccfD7 + ccfD8 + ccfD9;
        var ccfE10 = ccfE5 + ccfE6 + ccfE7 + ccfE8 + ccfE9;
        var ccfF10 = ccfF5 + ccfF6 + ccfF7 + ccfF8 + ccfF9;
        var ccfG10 = ccfG5 + ccfG6 + ccfG7 + ccfG8 + ccfG9;

        $('#ccfC10').val(ccfC10.toFixed(1));
        $('#ccfD10').val(ccfD10.toFixed(1));
        $('#ccfE10').val(ccfE10.toFixed(1));
        $('#ccfF10').val(ccfF10.toFixed(1));
        $('#ccfG10').val(ccfG10.toFixed(1));

        //now-T5
        var ccfB15 = waccB3;
        var ccfB16 = waccB4;
        var ccfB14 = ccfB16 - ccfB15;

        var ccfC15 = waccC3;
        var ccfC16 = waccC4;
        var ccfC14 = ccfC16 - ccfC15;

        var ccfD15 = waccD3;
        var ccfD16 = waccD4;
        var ccfD14 = ccfD16 - ccfD15;

        var ccfE15 = waccE3;
        var ccfE16 = waccE4;
        var ccfE14 = ccfE16 - ccfE15;

        var ccfF15 = waccF3;
        var ccfF16 = waccF4;
        var ccfF14 = ccfF16 - ccfF15;

        var ccfG15 = waccG3;
        var ccfG16 = waccG4;
        var ccfG14 = ccfG16 - ccfG15;

        $('#ccfB14').val(ccfB14.toFixed(1));
        $('#ccfB15').val(ccfB15.toFixed(1));
        $('#ccfB16').val(ccfB16.toFixed(1));

        $('#ccfC14').val(ccfC14.toFixed(1));
        $('#ccfC15').val(ccfC15.toFixed(1));
        $('#ccfC16').val(ccfC16.toFixed(1));

        $('#ccfD14').val(ccfD14.toFixed(1));
        $('#ccfD15').val(ccfD15.toFixed(1));
        $('#ccfD16').val(ccfD16.toFixed(1));

        $('#ccfE14').val(ccfE14.toFixed(1));
        $('#ccfE15').val(ccfE15.toFixed(1));
        $('#ccfE16').val(ccfE16.toFixed(1));

        $('#ccfF14').val(ccfF14.toFixed(1));
        $('#ccfF15').val(ccfF15.toFixed(1));
        $('#ccfF16').val(ccfF16.toFixed(1));

        $('#ccfG14').val(ccfG14.toFixed(1));
        $('#ccfG15').val(ccfG15.toFixed(1));
        $('#ccfG16').val(ccfG16.toFixed(1));

        // Financial Leverage (L)
        var ccfB18 = ccfB15 / ccfB14;
        var ccfC18 = ccfC15 / ccfC14;
        var ccfD18 = ccfD15 / ccfD14;
        var ccfE18 = ccfE15 / ccfE14;
        var ccfF18 = ccfF15 / ccfF14;
        $('#ccfB18').val(ccfB18.toFixed(3));
        $('#ccfC18').val(ccfC18.toFixed(3));
        $('#ccfD18').val(ccfD18.toFixed(3));
        $('#ccfE18').val(ccfE18.toFixed(3));
        $('#ccfF18').val(ccfF18.toFixed(3));

        // Percetage of Equity (We)
        var ccfB19 = ccfB14 / ccfB16;
        var ccfC19 = ccfC14 / ccfC16;
        var ccfD19 = ccfD14 / ccfD16;
        var ccfE19 = ccfE14 / ccfE16;
        var ccfF19 = ccfF14 / ccfF16;
        $('#ccfB19').val((ccfB19 * 100).toFixed(2));
        $('#ccfC19').val((ccfC19 * 100).toFixed(2));
        $('#ccfD19').val((ccfD19 * 100).toFixed(2));
        $('#ccfE19').val((ccfE19 * 100).toFixed(2));
        $('#ccfF19').val((ccfF19 * 100).toFixed(2));

        // Percentage of Debt (Wd)
        var ccfB20 = 1 - ccfB19;
        var ccfC20 = 1 - ccfC19;
        var ccfD20 = 1 - ccfD19;
        var ccfE20 = 1 - ccfE19;
        var ccfF20 = 1 - ccfF19;
        $('#ccfB20').val((ccfB20 * 100).toFixed(2));
        $('#ccfC20').val((ccfC20 * 100).toFixed(2));
        $('#ccfD20').val((ccfD20 * 100).toFixed(2));
        $('#ccfE20').val((ccfE20 * 100).toFixed(2));
        $('#ccfF20').val((ccfF20 * 100).toFixed(2));

        // Cost of Equity Unleveraged (Keu)
        var ccfB21 = assumptionsB42;
        var ccfC21 = ccfB21;
        var ccfD21 = ccfB21;
        var ccfE21 = ccfB21;
        var ccfF21 = ccfB21;
        $('#ccfB21').val((ccfB21 * 100).toFixed(2));
        $('#ccfC21').val((ccfC21 * 100).toFixed(2));
        $('#ccfD21').val((ccfD21 * 100).toFixed(2));
        $('#ccfE21').val((ccfE21 * 100).toFixed(2));
        $('#ccfF21').val((ccfF21 * 100).toFixed(2));

        var ccfB23 = assumptionsB34;
        var ccfC23 = ccfB23;
        var ccfD23 = ccfB23;
        var ccfE23 = ccfB23;
        var ccfF23 = ccfB23;
        $('#ccfB23').val((ccfB23 * 100).toFixed(2));
        $('#ccfC23').val((ccfC23 * 100).toFixed(2));
        $('#ccfD23').val((ccfD23 * 100).toFixed(2));
        $('#ccfE23').val((ccfE23 * 100).toFixed(2));
        $('#ccfF23').val((ccfF23 * 100).toFixed(2));

        // Cost of Equity Leveraged (Kel)
        var ccfB22 = ccfB21 + ccfB18 * (ccfB21 - ccfB23);
        var ccfC22 = ccfC21 + ccfC18 * (ccfC21 - ccfC23);
        var ccfD22 = ccfD21 + ccfD18 * (ccfD21 - ccfD23);
        var ccfE22 = ccfE21 + ccfE18 * (ccfE21 - ccfE23);
        var ccfF22 = ccfF21 + ccfF18 * (ccfF21 - ccfF23);
        $('#ccfB22').val((ccfB22 * 100).toFixed(2));
        $('#ccfC22').val((ccfC22 * 100).toFixed(2));
        $('#ccfD22').val((ccfD22 * 100).toFixed(2));
        $('#ccfE22').val((ccfE22 * 100).toFixed(2));
        $('#ccfF22').val((ccfF22 * 100).toFixed(2));

        // WACC (with out (1-t))
        var ccfB24 = (ccfB19 * ccfB22) + (ccfB20 * ccfB23);
        var ccfC24 = (ccfC19 * ccfC22) + (ccfC20 * ccfC23);
        var ccfD24 = (ccfD19 * ccfD22) + (ccfD20 * ccfD23);
        var ccfE24 = (ccfE19 * ccfE22) + (ccfE20 * ccfE23);
        var ccfF24 = (ccfF19 * ccfF22) + (ccfF20 * ccfF23);
        $('#ccfB24').val((ccfB24 * 100).toFixed(2));
        $('#ccfC24').val((ccfC24 * 100).toFixed(2));
        $('#ccfD24').val((ccfD24 * 100).toFixed(2));
        $('#ccfE24').val((ccfE24 * 100).toFixed(2));
        $('#ccfF24').val((ccfF24 * 100).toFixed(2));

        // Terminal Value (TV)
        var ccfG27 = fcfB15;
        $('#ccfG27').val(ccfG27.toFixed(1));

        // Total CCF
        var ccfC28 = ccfC10;
        var ccfD28 = ccfD10;
        var ccfE28 = ccfE10;
        var ccfF28 = ccfF10;
        var ccfG28 = ccfG10 + ccfG27;
        $('#ccfC28').val(ccfC28.toFixed(1));
        $('#ccfD28').val(ccfD28.toFixed(1));
        $('#ccfE28').val(ccfE28.toFixed(1));
        $('#ccfF28').val(ccfF28.toFixed(1));
        $('#ccfG28').val(ccfG28.toFixed(1));

        // Discount Factor (Fac)
        var ccfC29 = 1 / (1 + ccfB24);
        var ccfD29 = ccfC29 / (1 + ccfC24);
        var ccfE29 = ccfD29 / (1 + ccfD24);
        var ccfF29 = ccfE29 / (1 + ccfE24);
        var ccfG29 = ccfF29 / (1 + ccfF24);
        $('#ccfC29').val(ccfC29.toFixed(4));
        $('#ccfD29').val(ccfD29.toFixed(4));
        $('#ccfE29').val(ccfE29.toFixed(4));
        $('#ccfF29').val(ccfF29.toFixed(4));
        $('#ccfG29').val(ccfG29.toFixed(4));

        // Enterprise Value (EV)
        var ccfB30 = (ccfC28 * ccfC29) + (ccfD28 * ccfD29) + (ccfE28 * ccfE29) + (ccfF28 * ccfF29) + (ccfG28 * ccfG29);
        $('#ccfB30').val(ccfB30.toFixed(1));

        /**
         * calcula Accounting
         */
            // pre Balance
            // Acc Receivable
        var accountingB2 = initialDataC4;
        // Inventories
        var accountingB3 = initialDataC5;
        // Acc Payables
        var accountingB4 = initialDataC6;
        // Total Operat WC
        var accountingB5 = accountingB2 + accountingB3 + accountingB4;
        // Cash
        var accountingB6 = initialDataC7;
        // Net Fixed Assets
        // Total Net Assets
        var accountingB9 = accountingB5 + accountingB6 + accountingB7;

        // Debit
        var accountingB13 = accountingB11 + accountingB12;

        $('#accountingB2').val(accountingB2.toFixed(1));
        $('#accountingB3').val(accountingB3.toFixed(1));
        $('#accountingB4').val(accountingB4.toFixed(1));
        $('#accountingB5').val(accountingB5.toFixed(1));
        $('#accountingB6').val(accountingB6.toFixed(1));
        $('#accountingB7').val(accountingB7.toFixed(1));
        $('#accountingB9').val(accountingB9.toFixed(1));

        $('#accountingB11').val(accountingB11.toFixed(1));
        $('#accountingB12').val(accountingB12.toFixed(1));
        $('#accountingB13').val(accountingB13.toFixed(1));

        // Init Balance
        // Acc Receivable
        var accountingC2 = accountingB2;
        // Inventories
        var accountingC3 = accountingB3;
        // Acc Payables
        var accountingC4 = accountingB4;
        // Total Operat WC
        var accountingC5 = accountingB5;
        // Cash
        var accountingC6 = accountingB6;
        // Net Fixed Assets
        // Total Net Assets
        var accountingC9 = accountingC5 + accountingC6 + accountingC7 + accountingC8;

        $('#accountingC2').val(accountingC2.toFixed(1));
        $('#accountingC3').val(accountingC3.toFixed(1));
        $('#accountingC4').val(accountingC4.toFixed(1));
        $('#accountingC5').val(accountingC5.toFixed(1));
        $('#accountingC6').val(accountingC6.toFixed(1));
        $('#accountingC7').val(accountingC7.toFixed(1));
        $('#accountingC8').val(accountingC8.toFixed(1));
        $('#accountingC9').val(accountingC9.toFixed(1));

        $('#accountingC11').val(accountingC11.toFixed(1));
        $('#accountingC12').val(accountingC12.toFixed(1));
        $('#accountingC13').val(accountingC13.toFixed(1));

        var accountingD10 = accountingD9 - accountingD13;

        $('#accountingD2').val(accountingD2.toFixed(1));
        $('#accountingD3').val(accountingD3.toFixed(1));
        $('#accountingD4').val(accountingD4.toFixed(1));
        $('#accountingD5').val(accountingD5.toFixed(1));
        $('#accountingD6').val(accountingD6.toFixed(1));
        $('#accountingD7').val(accountingD7.toFixed(1));
        $('#accountingD8').val(accountingD8.toFixed(1));
        $('#accountingD9').val(accountingD9.toFixed(1));

        $('#accountingD10').val(accountingD10.toFixed(1));
        $('#accountingD11').val(accountingD11.toFixed(1));
        $('#accountingD12').val(accountingD12.toFixed(1));
        $('#accountingD13').val(accountingD13.toFixed(1));

        // T2
        var accountingE10 = accountingE9 - accountingE13;

        $('#accountingE2').val(accountingE2.toFixed(1));
        $('#accountingE3').val(accountingE3.toFixed(1));
        $('#accountingE4').val(accountingE4.toFixed(1));
        $('#accountingE5').val(accountingE5.toFixed(1));
        $('#accountingE6').val(accountingE6.toFixed(1));
        $('#accountingE7').val(accountingE7.toFixed(1));
        $('#accountingE8').val(accountingE8.toFixed(1));
        $('#accountingE9').val(accountingE9.toFixed(1));

        $('#accountingE10').val(accountingE10.toFixed(1));
        $('#accountingE11').val(accountingE11.toFixed(1));
        $('#accountingE12').val(accountingE12.toFixed(1));
        $('#accountingE13').val(accountingE13.toFixed(1));

        // T3
        var accountingF10 = accountingF9 - accountingF13;

        $('#accountingF2').val(accountingF2.toFixed(1));
        $('#accountingF3').val(accountingF3.toFixed(1));
        $('#accountingF4').val(accountingF4.toFixed(1));
        $('#accountingF5').val(accountingF5.toFixed(1));
        $('#accountingF6').val(accountingF6.toFixed(1));
        $('#accountingF7').val(accountingF7.toFixed(1));
        $('#accountingF8').val(accountingF8.toFixed(1));
        $('#accountingF9').val(accountingF9.toFixed(1));

        $('#accountingF10').val(accountingF10.toFixed(1));
        $('#accountingF11').val(accountingF11.toFixed(1));
        $('#accountingF12').val(accountingF12.toFixed(1));
        $('#accountingF13').val(accountingF13.toFixed(1));

        // T4
        var accountingG10 = accountingG9 - accountingG13;

        $('#accountingG2').val(accountingG2.toFixed(1));
        $('#accountingG3').val(accountingG3.toFixed(1));
        $('#accountingG4').val(accountingG4.toFixed(1));
        $('#accountingG5').val(accountingG5.toFixed(1));
        $('#accountingG6').val(accountingG6.toFixed(1));
        $('#accountingG7').val(accountingG7.toFixed(1));
        $('#accountingG8').val(accountingG8.toFixed(1));
        $('#accountingG9').val(accountingG9.toFixed(1));

        $('#accountingG10').val(accountingG10.toFixed(1));
        $('#accountingG11').val(accountingG11.toFixed(1));
        $('#accountingG12').val(accountingG12.toFixed(1));
        $('#accountingG13').val(accountingG13.toFixed(1));

        // T5
        var accountingH10 = accountingH9 - accountingH13;

        $('#accountingH2').val(accountingH2.toFixed(1));
        $('#accountingH3').val(accountingH3.toFixed(1));
        $('#accountingH4').val(accountingH4.toFixed(1));
        $('#accountingH5').val(accountingH5.toFixed(1));
        $('#accountingH6').val(accountingH6.toFixed(1));
        $('#accountingH7').val(accountingH7.toFixed(1));
        $('#accountingH9').val(accountingH9.toFixed(1));

        $('#accountingH10').val(accountingH10.toFixed(1));
        $('#accountingH11').val(accountingH11.toFixed(1));
        $('#accountingH12').val(accountingH12.toFixed(1));
        $('#accountingH13').val(accountingH13.toFixed(1));

        // T1
        // To share Holders
        var accountingD32 = evcForShareholdersC7;
        // To Reserves
        var accountingD33 = accountingD30 - accountingD32;

        $('#accountingD22').val(accountingD22.toFixed(1));
        $('#accountingD23').val(accountingD23.toFixed(1));
        $('#accountingD24').val(accountingD24.toFixed(1));
        $('#accountingD25').val(accountingD25.toFixed(1));
        $('#accountingD26').val(accountingD26.toFixed(1));
        $('#accountingD27').val(accountingD27.toFixed(1));
        $('#accountingD28').val(accountingD28.toFixed(1));
        $('#accountingD29').val(accountingD29.toFixed(1));
        $('#accountingD30').val(accountingD30.toFixed(1));

        $('#accountingD32').val(accountingD32.toFixed(1));
        $('#accountingD33').val(accountingD33.toFixed(1));

        // T2
        // To share Holders
        var accountingE32 = evcForShareholdersD7;
        // To Reserves
        var accountingE33 = accountingE30 - accountingE32;

        $('#accountingE22').val(accountingE22.toFixed(1));
        $('#accountingE23').val(accountingE23.toFixed(1));
        $('#accountingE24').val(accountingE24.toFixed(1));
        $('#accountingE25').val(accountingE25.toFixed(1));
        $('#accountingE26').val(accountingE26.toFixed(1));
        $('#accountingE27').val(accountingE27.toFixed(1));
        $('#accountingE28').val(accountingE28.toFixed(1));
        $('#accountingE29').val(accountingE29.toFixed(1));
        $('#accountingE30').val(accountingE30.toFixed(1));

        $('#accountingE32').val(accountingE32.toFixed(1));
        $('#accountingE33').val(accountingE33.toFixed(1));

        // T3
        // To share Holders
        var accountingF32 = evcForShareholdersE7;
        // To Reserves
        var accountingF33 = accountingF30 - accountingF32;

        $('#accountingF22').val(accountingF22.toFixed(1));
        $('#accountingF23').val(accountingF23.toFixed(1));
        $('#accountingF24').val(accountingF24.toFixed(1));
        $('#accountingF25').val(accountingF25.toFixed(1));
        $('#accountingF26').val(accountingF26.toFixed(1));
        $('#accountingF27').val(accountingF27.toFixed(1));
        $('#accountingF28').val(accountingF28.toFixed(1));
        $('#accountingF29').val(accountingF29.toFixed(1));
        $('#accountingF30').val(accountingF30.toFixed(1));

        $('#accountingF32').val(accountingF32.toFixed(1));
        $('#accountingF33').val(accountingF33.toFixed(1));

        // T4
        // Sales
        // To share Holders
        var accountingG32 = evcForShareholdersF7;
        // To Reserves
        var accountingG33 = accountingG30 - accountingG32;

        $('#accountingG22').val(accountingG22.toFixed(1));
        $('#accountingG23').val(accountingG23.toFixed(1));
        $('#accountingG24').val(accountingG24.toFixed(1));
        $('#accountingG25').val(accountingG25.toFixed(1));
        $('#accountingG26').val(accountingG26.toFixed(1));
        $('#accountingG27').val(accountingG27.toFixed(1));
        $('#accountingG28').val(accountingG28.toFixed(1));
        $('#accountingG29').val(accountingG29.toFixed(1));
        $('#accountingG30').val(accountingG30.toFixed(1));

        $('#accountingG32').val(accountingG32.toFixed(1));
        $('#accountingG33').val(accountingG33.toFixed(1));

        // T5
        // To share Holders
        var accountingH32 = evcForShareholdersG7 - evcForShareholdersG6;
        // To Reserves
        var accountingH33 = accountingH30 - accountingH32;

        $('#accountingH22').val(accountingH22.toFixed(1));
        $('#accountingH23').val(accountingH23.toFixed(1));
        $('#accountingH24').val(accountingH24.toFixed(1));
        $('#accountingH25').val(accountingH25.toFixed(1));
        $('#accountingH26').val(accountingH26.toFixed(1));
        $('#accountingH27').val(accountingH27.toFixed(1));
        $('#accountingH28').val(accountingH28.toFixed(1));
        $('#accountingH29').val(accountingH29.toFixed(1));
        $('#accountingH30').val(accountingH30.toFixed(1));

        $('#accountingH32').val(accountingH32.toFixed(1));
        $('#accountingH33').val(accountingH33.toFixed(1));

        // T1
        // Initial E
        var accountingD16 = accountingC12;
        var accountingD17 = accountingD30;
        var accountingD18 = -1 * accountingD32;
        var accountingD19 = accountingD16 + accountingD17 + accountingD18;
        var accountingD14 = accountingD19 - accountingD12;

        $('#accountingD14').val(accountingD14.toFixed(1));
        $('#accountingD16').val(accountingD16.toFixed(1));
        $('#accountingD17').val(accountingD17.toFixed(1));
        $('#accountingD18').val(accountingD18.toFixed(1));
        $('#accountingD19').val(accountingD19.toFixed(1));

        // T2
        // Initial E
        var accountingE16 = accountingD19;
        var accountingE17 = accountingE30;
        var accountingE18 = -1 * accountingE32;
        var accountingE19 = accountingE16 + accountingE17 + accountingE18;
        var accountingE14 = accountingE19 - accountingE12;

        $('#accountingE14').val(accountingE14.toFixed(1));
        $('#accountingE16').val(accountingE16.toFixed(1));
        $('#accountingE17').val(accountingE17.toFixed(1));
        $('#accountingE18').val(accountingE18.toFixed(1));
        $('#accountingE19').val(accountingE19.toFixed(1));

        // T3
        // Initial E
        var accountingF16 = accountingE19;
        var accountingF17 = accountingF30;
        var accountingF18 = -1 * accountingF32;
        var accountingF19 = accountingF16 + accountingF17 + accountingF18;
        var accountingF14 = accountingF19 - accountingF12;

        $('#accountingF14').val(accountingF14.toFixed(1));
        $('#accountingF16').val(accountingF16.toFixed(1));
        $('#accountingF17').val(accountingF17.toFixed(1));
        $('#accountingF18').val(accountingF18.toFixed(1));
        $('#accountingF19').val(accountingF19.toFixed(1));

        // T4
        // Initial E
        var accountingG16 = accountingF19;
        var accountingG17 = accountingG30;
        var accountingG18 = -1 * accountingG32;
        var accountingG19 = accountingG16 + accountingG17 + accountingG18;
        var accountingG14 = accountingG19 - accountingG12;

        $('#accountingG14').val(accountingG14.toFixed(1));
        $('#accountingG16').val(accountingG16.toFixed(1));
        $('#accountingG17').val(accountingG17.toFixed(1));
        $('#accountingG18').val(accountingG18.toFixed(1));
        $('#accountingG19').val(accountingG19.toFixed(1));

        // T5
        // Initial E
        var accountingH16 = accountingG19;
        var accountingH17 = accountingH30;
        var accountingH18 = -1 * accountingH32;
        var accountingH19 = accountingH16 + accountingH17 + accountingH18;
        var accountingH14 = accountingH19 - accountingH12;

        $('#accountingH14').val(accountingH14.toFixed(1));
        $('#accountingH16').val(accountingH16.toFixed(1));
        $('#accountingH17').val(accountingH17.toFixed(1));
        $('#accountingH18').val(accountingH18.toFixed(1));
        $('#accountingH19').val(accountingH19.toFixed(1));

        // purchases
        var accountingD36 = accountingD3 - accountingD23 - accountingB3;
        var accountingE36 = accountingE3 - accountingE23 - accountingD3;
        var accountingF36 = accountingF3 - accountingF23 - accountingE3;
        var accountingG36 = accountingG3 - accountingG23 - accountingF3;
        var accountingH36 = accountingH3 - accountingH23 - accountingG3;

        $('#accountingD36').val(accountingD36.toFixed(1));
        $('#accountingE36').val(accountingE36.toFixed(1));
        $('#accountingF36').val(accountingF36.toFixed(1));
        $('#accountingG36').val(accountingG36.toFixed(1));
        $('#accountingH36').val(accountingH36.toFixed(1));

        //ROA
        var accountingE39 = accountingE30 / accountingE9;
        var accountingF39 = accountingF30 / accountingF9;
        var accountingG39 = accountingG30 / accountingG9;
        var accountingH39 = accountingH30 / accountingH9;
        var accountingI39 = (accountingD39 + accountingE39 + accountingF39 + accountingG39 + accountingH39) / 5;
        var evcForShareholdersB22 = accountingI39;

        $('#accountingD39').val((accountingD39 * 100).toFixed(2));
        $('#accountingE39').val((accountingE39 * 100).toFixed(2));
        $('#accountingF39').val((accountingF39 * 100).toFixed(2));
        $('#accountingG39').val((accountingG39 * 100).toFixed(2));
        $('#accountingH39').val((accountingH39 * 100).toFixed(2));
        $('#accountingI39').val((accountingI39 * 100).toFixed(2));

        //ROE
        var accountingE40 = accountingE30 / accountingE12;
        var accountingF40 = accountingF30 / accountingF12;
        var accountingG40 = accountingG30 / accountingG12;
        var accountingH40 = accountingH30 / accountingH12;
        var accountingI40 = (accountingD40 + accountingE40 + accountingF40 + accountingG40 + accountingH40) / 5;
        var evcForShareholdersB23 = accountingI40;
        $('#evcForShareholdersB22').val((evcForShareholdersB22 * 100).toFixed(2));
        $('#evcForShareholdersB23').val((evcForShareholdersB23 * 100).toFixed(2));

        $('#accountingD40').val((accountingD40 * 100).toFixed(2));
        $('#accountingE40').val((accountingE40 * 100).toFixed(2));
        $('#accountingF40').val((accountingF40 * 100).toFixed(2));
        $('#accountingG40').val((accountingG40 * 100).toFixed(2));
        $('#accountingH40').val((accountingH40 * 100).toFixed(2));
        $('#accountingI40').val((accountingI40 * 100).toFixed(2));

        resolve(true);
    });
}

/**
 * inicia todos los cálculos
 */
function startCalculateData() {
    getInitializeData().then(function (initialdata) {
        initialData = initialdata;
        calculateData().then(function() {
            $('.pricing').removeAttr('disabled');
            changeColors();
        });
    });

}

var dictionary = "01234".split('');
var gen;

/**
 *
 * @returns {number}
 */
function ran() {
    return Math.floor(Math.random() * dictionary.length);
}
/**
 *
 * @param amt
 * @returns {string}
 */
function ranString(amt) {
    var string = '';
    for(var i = 0; i < amt; i++) {
        string += dictionary[ran()];
    }
    return string;
}
/**
 *
 */
function testAnimation() {
    $('.ev').html('');
    $('.equity').html('');
    if (typeof gen === 'undefined') {
        var str = dictionary;
        var count = str.length;
        var delay = 20;
        var algen = setInterval(function() {
            $('.ev').attr('data-before', ranString(count));
            $('.equity').attr('data-before', ranString(count));

            $('.equity').html('');
            $('.ev').html('');
            if(delay > 0) {
                delay--;
            }
            else {
                if (count >= str.length) {
                    clearInterval(algen);
                    $('.ev').attr('data-before', '');
                    $('.equity').attr('data-before', '');
                    $('.equity').html(fcfB21.toFixed(1));
                    $('.ev').html(fcfB19.toFixed(1));
                }
                count--;
            }
        }, 32);
    }
}
/**
 * compara el campo waccB4 con el campo fcfB19 y
 * determina si se debe corregir y recalcular
 */
function pricing() {
    testAnimation();
    var difference = Math.abs(Math.abs(fcfB19) - Math.abs(waccB4));
    var differenceFixed = difference.toFixed(4);
    if (differenceFixed !== '0.0001' && differenceFixed !=='0.0000') {
        waccB4 = Math.abs(fcfB19);

        calculateData().then(function() {
            pricing();
        });
    } else {
        calculateMultiples();
        $('#waccB4').val(waccB4.toFixed(1));
        $('.equity').html(fcfB21.toFixed(1));
        $('.ev').html(fcfB19.toFixed(1));
        changeColors();
        clearInterval(gen);
        delete gen;
    }
}

function calculateMultiples() {
    // evSales
    var initialDataC17_2 = fcfB19 / initialData.initialDataC17;
    $('#initialDataC17-2').val(initialDataC17_2.toFixed(1));
    // EBIT
    var initialDataC19 = initialData.initialDataC17 + initialData.initialDataC18;
    var initialDataC22 = initialDataC19 + initialData.initialDataC20 + initialData.initialDataC21;

    // evEbit
    var initialDataC21_2 = fcfB19 / initialDataC22;
    $('#initialDataC21-2').val(initialDataC21_2.toFixed(1));
    // evEBITDA
    var initialDataC22_2 = fcfB19 / (initialDataC22 + initialData.initialDataC21);
    $('#initialDataC22-2').val(initialDataC22_2.toFixed(1));
    // evBookvalue
    var initialDataC36 = initialData.assumptionsB22;
    var initialDataC37 = initialDataC36 * initialData.initialDataC17;
    var initialDataC7 = initialDataC37;
    var initialDataC8 = initialData.initialDataC4 + initialData.initialDataC5 + initialData.initialDataC6 + initialDataC7;
    var initialDataC10 = initialDataC8 + initialData.initialDataC9;
    var initialDataC14 = initialDataC10;
    var initialDataC13 = initialDataC14 - initialData.initialDataC12;

    var initialDataC13_2 = fcfB21 / initialDataC13;

    $('#initialDataC13-2').val(initialDataC13_2.toFixed(1));
}
/**
 * llama las funciones de cambio de color segun su tipo.
 */
function changeColors() {
    changeColorWritableInputs();
    changeColorReadOnlyInputs();
}
/**
 * cambia el color de los inputs en los q podemos escribir.
 */
function changeColorWritableInputs() {
    var inputs = $('input[type="text"]');
    var count = inputs.length;
    for(var i = 0; i < count; i++){
        $(inputs[i]).removeClass('ngtv');
        if($(inputs[i]).val().replace(',','.') < 0) {
            $(inputs[i]).addClass('ngtv');
        }
    }
}
/**
 * cambia el color de los inputs de solo lectura.
 */
function changeColorReadOnlyInputs() {
    var inputsReadOnly = $('input[readonly=readonly]');
    var countReadOnly = inputsReadOnly.length;
    for(var j = 0; j < countReadOnly; j++){
        $(inputsReadOnly[j]).removeClass('ngtv');
        if($(inputsReadOnly[j]).val() < 0) {
            $(inputsReadOnly[j]).addClass('ngtv');
        }
    }
}
