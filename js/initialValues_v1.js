function initializeData() {
    console.info('inicializando');
    /**
     * Variables iniciales de Initialdata
     */
    $('#initialDataC4').val(249);
    $('#initialDataC5').val(93);
    $('#initialDataC6').val(-43);
    $('#initialDataC9').val(3000);
    $('#initialDataC12').val(800);
    $('#initialDataC17').val(1300);
    $('#initialDataC18').val(-520);
    $('#initialDataC20').val(-260);
    $('#initialDataC21').val(-100);
    $('#initialDataC23').val(-126);
    $('#initialDataC35').val(200);

    /**
     * Variables iniciales de Assumptions
     */
    $('#assumptionsB2').val(1500.0.toFixed(1));
    $('#assumptionsB7').val(3.0.toFixed(1));
    $('#assumptionsB16').val(105.0.toFixed(1));
    $('#assumptionsB17').val(3.0.toFixed(1));
    $('#assumptionsB22').val(5.0.toFixed(1));
    $('#assumptionsB34').val(6.0.toFixed(1));
    $('#assumptionsB35').val(4.0.toFixed(1));
    $('#assumptionsB36').val(4.0.toFixed(1));
    $('#assumptionsB37').val(1.1.toFixed(1));
    $('#assumptionsB38').val(30.0.toFixed(1));
    $('#assumptionsB39').val(2.0.toFixed(1));
    $('#assumptionsB40').val(8.0.toFixed(1));

    $('#assumptionsC14').val(40.0.toFixed(1));
    $('#assumptionsC15').val(20.0.toFixed(1));
    $('#assumptionsC19').val(60.0.toFixed(1));
    $('#assumptionsC20').val(60.0.toFixed(1));
    $('#assumptionsC21').val(30.0.toFixed(1));
    $('#assumptionsC28').val(30.0.toFixed(1));
    $('#assumptionsC29').val(2600.0.toFixed(1));
    $('#assumptionsC32').val(-100.0.toFixed(1));

    $('#assumptionsD11').val(6700.0.toFixed(1));
    $('#assumptionsD14').val(39.5.toFixed(1));
    $('#assumptionsD15').val(19.5.toFixed(1));
    $('#assumptionsD19').val(55.0.toFixed(1));
    $('#assumptionsD20').val(55.0.toFixed(1));
    $('#assumptionsD21').val(30.0.toFixed(1));
    $('#assumptionsD28').val(30.0.toFixed(1));
    $('#assumptionsD29').val(2400.0.toFixed(1));
    $('#assumptionsD32').val(0.0.toFixed(1));

    $('#assumptionsE11').val(7000.0.toFixed(1));
    $('#assumptionsE14').val(39.0.toFixed(1));
    $('#assumptionsE15').val(19.0.toFixed(1));
    $('#assumptionsE19').val(50.0.toFixed(1));
    $('#assumptionsE20').val(50.0.toFixed(1));
    $('#assumptionsE21').val(30.0.toFixed(1));
    $('#assumptionsE28').val(30.0.toFixed(1));
    $('#assumptionsE29').val(2200.0.toFixed(1));
    $('#assumptionsE32').val(-100.0.toFixed(1));

    $('#assumptionsF11').val(7400.0.toFixed(1));
    $('#assumptionsF14').val(38.5.toFixed(1));
    $('#assumptionsF15').val(18.5.toFixed(1));
    $('#assumptionsF19').val(45.0.toFixed(1));
    $('#assumptionsF20').val(45.0.toFixed(1));
    $('#assumptionsF21').val(30.0.toFixed(1));
    $('#assumptionsF28').val(30.0.toFixed(1));
    $('#assumptionsF29').val(2000.0.toFixed(1));
    $('#assumptionsF32').val(0.0.toFixed(1));

    $('#assumptionsG11').val(7800.0.toFixed(1));
    $('#assumptionsG14').val(38.0.toFixed(1));
    $('#assumptionsG15').val(18.0.toFixed(1));
    $('#assumptionsG19').val(40.0.toFixed(1));
    $('#assumptionsG20').val(40.0.toFixed(1));
    $('#assumptionsG21').val(30.0.toFixed(1));
    $('#assumptionsG28').val(30.0.toFixed(1));
    $('#assumptionsG29').val(1800.0.toFixed(1));
    $('#assumptionsG32').val(-100.0.toFixed(1));

    /**
     * Variables iniciales de EVCforShareholders
     */
    $('#evcForShareholdersB13').val(-3000);
    $('#evcForShareholdersB14').val(0.0.toFixed(1));
}

function getInitializeData() {
    return new Promise(function (resolve) {
        console.info('cargando variables');
        var response = {
            /**
             * Variables iniciales de Initialdata
             */
            initialDataC4: Number($('#initialDataC4').val().replace(',','.')),
            initialDataC5: Number($('#initialDataC5').val().replace(',','.')),
            initialDataC6: Number($('#initialDataC6').val().replace(',','.')),
            initialDataC9: Number($('#initialDataC9').val().replace(',','.')),
            initialDataC12: Number($('#initialDataC12').val().replace(',','.')),
            initialDataC17: Number($('#initialDataC17').val().replace(',','.')),
            initialDataC18: Number($('#initialDataC18').val().replace(',','.')),
            initialDataC20: Number($('#initialDataC20').val().replace(',','.')),
            initialDataC21: Number($('#initialDataC21').val().replace(',','.')),
            initialDataC23: Number($('#initialDataC23').val().replace(',','.')),
            initialDataC35: Number($('#initialDataC35').val().replace(',','.')),

            /**
             * Variables iniciales de Assumptions
             */
            assumptionsB2: Number($('#assumptionsB2').val().replace(',','.')),
            assumptionsB7: Number($('#assumptionsB7').val().replace(',','.')) / 100,
            assumptionsB16: Number($('#assumptionsB16').val().replace(',','.')),
            assumptionsB17: Number($('#assumptionsB17').val().replace(',','.')) / 100,
            assumptionsB22: Number($('#assumptionsB22').val().replace(',','.')) / 100,
            assumptionsB34: Number($('#assumptionsB34').val().replace(',','.')) / 100,
            assumptionsB35: Number($('#assumptionsB35').val().replace(',','.')) / 100,
            assumptionsB36: Number($('#assumptionsB36').val().replace(',','.')) / 100,
            assumptionsB37: Number($('#assumptionsB37').val().replace(',','.')),
            assumptionsB38: Number($('#assumptionsB38').val().replace(',','.')) / 100,
            assumptionsB39: Number($('#assumptionsB39').val().replace(',','.')) / 100,
            assumptionsB40: Number($('#assumptionsB40').val().replace(',','.')) / 100,

            assumptionsC14: Number($('#assumptionsC14').val().replace(',','.')) / 100,
            assumptionsC15: Number($('#assumptionsC15').val().replace(',','.')) / 100,
            assumptionsC19: Number($('#assumptionsC19').val().replace(',','.')),
            assumptionsC20: Number($('#assumptionsC20').val().replace(',','.')),
            assumptionsC21: Number($('#assumptionsC21').val().replace(',','.')),
            assumptionsC28: Number($('#assumptionsC28').val().replace(',','.')) / 100,
            assumptionsC29: Number($('#assumptionsC29').val().replace(',','.')),
            assumptionsC32: Number($('#assumptionsC32').val().replace(',','.')),

            assumptionsD11: Number($('#assumptionsD11').val().replace(',','.')),
            assumptionsD14: Number($('#assumptionsD14').val().replace(',','.')) / 100,
            assumptionsD15: Number($('#assumptionsD15').val().replace(',','.')) / 100,
            assumptionsD19: Number($('#assumptionsD19').val().replace(',','.')),
            assumptionsD20: Number($('#assumptionsD20').val().replace(',','.')),
            assumptionsD21: Number($('#assumptionsD21').val().replace(',','.')),
            assumptionsD28: Number($('#assumptionsD28').val().replace(',','.')) / 100,
            assumptionsD29: Number($('#assumptionsD29').val().replace(',','.')),
            assumptionsD32: Number($('#assumptionsD32').val().replace(',','.')),

            assumptionsE11: Number($('#assumptionsE11').val().replace(',','.')),
            assumptionsE14: Number($('#assumptionsE14').val().replace(',','.')) / 100,
            assumptionsE15: Number($('#assumptionsE15').val().replace(',','.')) / 100,
            assumptionsE19: Number($('#assumptionsE19').val().replace(',','.')),
            assumptionsE20: Number($('#assumptionsE20').val().replace(',','.')),
            assumptionsE21: Number($('#assumptionsE21').val().replace(',','.')),
            assumptionsE28: Number($('#assumptionsE28').val().replace(',','.')) / 100,
            assumptionsE29: Number($('#assumptionsE29').val().replace(',','.')),
            assumptionsE32: Number($('#assumptionsE32').val().replace(',','.')),

            assumptionsF11: Number($('#assumptionsF11').val().replace(',','.')),
            assumptionsF14: Number($('#assumptionsF14').val().replace(',','.')) / 100,
            assumptionsF15: Number($('#assumptionsF15').val().replace(',','.')) / 100,
            assumptionsF19: Number($('#assumptionsF19').val().replace(',','.')),
            assumptionsF20: Number($('#assumptionsF20').val().replace(',','.')),
            assumptionsF21: Number($('#assumptionsF21').val().replace(',','.')),
            assumptionsF28: Number($('#assumptionsF28').val().replace(',','.')) / 100,
            assumptionsF29: Number($('#assumptionsF29').val().replace(',','.')),
            assumptionsF32: Number($('#assumptionsF32').val().replace(',','.')),

            assumptionsG11: Number($('#assumptionsG11').val().replace(',','.')),
            assumptionsG14: Number($('#assumptionsG14').val().replace(',','.')) / 100,
            assumptionsG15: Number($('#assumptionsG15').val().replace(',','.')) / 100,
            assumptionsG19: Number($('#assumptionsG19').val().replace(',','.')),
            assumptionsG20: Number($('#assumptionsG20').val().replace(',','.')),
            assumptionsG21: Number($('#assumptionsG21').val().replace(',','.')),
            assumptionsG28: Number($('#assumptionsG28').val().replace(',','.')) / 100,
            assumptionsG29: Number($('#assumptionsG29').val().replace(',','.')),
            assumptionsG32: Number($('#assumptionsG32').val().replace(',','.')),

            /**
             * Variables iniciales de EVCforShareholders
             */
            evcForShareholdersB13: Number($('#evcForShareholdersB13').val().replace(',','.')),
            evcForShareholdersB14: Number($('#evcForShareholdersB14').val().replace(',','.'))
        };
        resolve(response);
    });
}

