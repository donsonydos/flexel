$(document).ready(function () {
    initializeData();

    $('.titleFixed').click(function () {
        $('#title').html($(this).attr('title'));
    });

    var shiftPresset = false;

    // eventos de navegacion co la tebla tab
    $(
        '#assumptionsB41, #assumptionsB42, ' +
        '#waccB5, ' +
        '#waccB6, #waccC6, #waccD6, #waccE6, #waccF6, #waccG6, ' +
        '#waccB8, #waccC8, #waccD8, #waccE8, #waccF8, ' +
        '#waccB9, #waccC9, #waccD9, #waccE9, #waccF9, ' +
        '#waccB10, #waccC10, #waccD10, #waccE10, #waccF10, ' +
        '#waccB11, #waccC11, #waccD11, #waccE11, #waccF11, ' +
        '#waccB12, #waccC12, #waccD12, #waccE12, #waccF12, ' +
        '#evcForShareholdersB22, #evcForShareholdersB23, ' +
        '#ccfB18, #ccfC18, #ccfD18, #ccfE18, #ccfF18, ' +
        '#ccfB19, #ccfC19, #ccfD19, #ccfE19, #ccfF19, ' +
        '#ccfB20, #ccfC20, #ccfD20, #ccfE20, #ccfF20, ' +
        '#ccfB21, #ccfC21, #ccfD21, #ccfE21, #ccfF21, ' +
        '#ccfB22, #ccfC22, #ccfD22, #ccfE22, #ccfF22, ' +
        '#ccfB23, #ccfC23, #ccfD23, #ccfE23, #ccfF23, ' +
        '#ccfB24, #ccfC24, #ccfD24, #ccfE24, #ccfF24, ' +
        '#accountingD39, #accountingE39, #accountingF39, #accountingG39, #accountingH39, #accountingI39, ' +
        '#accountingD40, #accountingE40, #accountingF40, #accountingG40, #accountingH40, #accountingI40 '
    ).keydown(function (event) {
        var keyCode = event.keyCode;
        var id = $(this).attr('id');
        if (keyCode === 9) {

            if (id === 'accountingI40' && shiftPresset) {
                $('#accountingH40').focus()
            }
            if (id === 'accountingH40') {
                if (shiftPresset) {
                    $('#accountingG40').focus()
                } else {
                    $('#accountingI40').focus()
                }
            }
            if (id === 'accountingG40') {
                if (shiftPresset) {
                    $('#accountingF40').focus()
                } else {
                    $('#accountingH40').focus()
                }
            }
            if (id === 'accountingF40') {
                if (shiftPresset) {
                    $('#accountingE40').focus()
                } else {
                    $('#accountingG40').focus()
                }
            }
            if (id === 'accountingE40') {
                if (shiftPresset) {
                    $('#accountingD40').focus()
                } else {
                    $('#accountingF40').focus()
                }
            }
            if (id === 'accountingD40') {
                if (shiftPresset) {
                    $('#accountingI39').focus()
                } else {
                    $('#accountingE40').focus()
                }
            }
            if (id === 'accountingI39') {
                if (shiftPresset) {
                    $('#accountingH39').focus()
                } else {
                    $('#accountingD40').focus()
                }
            }
            if (id === 'accountingH39') {
                if (shiftPresset) {
                    $('#accountingG39').focus()
                } else {
                    $('#accountingI39').focus()
                }
            }
            if (id === 'accountingG39') {
                if (shiftPresset) {
                    $('#accountingF39').focus()
                } else {
                    $('#accountingH39').focus()
                }
            }
            if (id === 'accountingF39') {
                if (shiftPresset) {
                    $('#accountingE39').focus()
                } else {
                    $('#accountingG39').focus()
                }
            }
            if (id === 'accountingE39') {
                if (shiftPresset) {
                    $('#accountingD39').focus()
                } else {
                    $('#accountingF39').focus()
                }
            }
            if (id === 'accountingD39') {
                if (shiftPresset) {
                    $('#accountingH36').focus()
                } else {
                    $('#accountingE39').focus()
                }
            }
            if (id === 'ccfF24') {
                if (shiftPresset) {
                    $('#ccfE24').focus()
                } else {
                    $('#ccfG27').focus()
                }
            }
            if (id === 'ccfE24') {
                if (shiftPresset) {
                    $('#ccfD24').focus()
                } else {
                    $('#ccfF24').focus()
                }
            }
            if (id === 'ccfD24') {
                if (shiftPresset) {
                    $('#ccfC24').focus()
                } else {
                    $('#ccfE24').focus()
                }
            }
            if (id === 'ccfC24') {
                if (shiftPresset) {
                    $('#ccfB24').focus()
                } else {
                    $('#ccfD24').focus()
                }
            }
            if (id === 'ccfB24') {
                if (shiftPresset) {
                    $('#ccfF23').focus()
                } else {
                    $('#ccfC24').focus()
                }
            }
            if (id === 'ccfF23') {
                if (shiftPresset) {
                    $('#ccfE23').focus()
                } else {
                    $('#ccfB24').focus()
                }
            }
            if (id === 'ccfE23') {
                if (shiftPresset) {
                    $('#ccfD23').focus()
                } else {
                    $('#ccfF23').focus()
                }
            }
            if (id === 'ccfD23') {
                if (shiftPresset) {
                    $('#ccfC23').focus()
                } else {
                    $('#ccfE23').focus()
                }
            }
            if (id === 'ccfC23') {
                if (shiftPresset) {
                    $('#ccfB23').focus()
                } else {
                    $('#ccfD23').focus()
                }
            }
            if (id === 'ccfB23') {
                if (shiftPresset) {
                    $('#ccfF22').focus()
                } else {
                    $('#ccfC23').focus()
                }
            }
            if (id === 'ccfF22') {
                if (shiftPresset) {
                    $('#ccfE22').focus()
                } else {
                    $('#ccfB23').focus()
                }
            }
            if (id === 'ccfE22') {
                if (shiftPresset) {
                    $('#ccfD22').focus()
                } else {
                    $('#ccfF22').focus()
                }
            }
            if (id === 'ccfD22') {
                if (shiftPresset) {
                    $('#ccfC22').focus()
                } else {
                    $('#ccfE22').focus()
                }
            }
            if (id === 'ccfC22') {
                if (shiftPresset) {
                    $('#ccfB22').focus()
                } else {
                    $('#ccfD22').focus()
                }
            }
            if (id === 'ccfB22') {
                if (shiftPresset) {
                    $('#ccfF21').focus()
                } else {
                    $('#ccfC22').focus()
                }
            }
            if (id === 'ccfF21') {
                if (shiftPresset) {
                    $('#ccfE21').focus()
                } else {
                    $('#ccfB22').focus()
                }
            }
            if (id === 'ccfE21') {
                if (shiftPresset) {
                    $('#ccfD21').focus()
                } else {
                    $('#ccfF21').focus()
                }
            }
            if (id === 'ccfD21') {
                if (shiftPresset) {
                    $('#ccfC21').focus()
                } else {
                    $('#ccfE21').focus()
                }
            }
            if (id === 'ccfC21') {
                if (shiftPresset) {
                    $('#ccfB21').focus()
                } else {
                    $('#ccfD21').focus()
                }
            }
            if (id === 'ccfB21') {
                if (shiftPresset) {
                    $('#ccfF20').focus()
                } else {
                    $('#ccfC21').focus()
                }
            }
            if (id === 'ccfF20') {
                if (shiftPresset) {
                    $('#ccfE20').focus()
                } else {
                    $('#ccfB21').focus()
                }
            }
            if (id === 'ccfE20') {
                if (shiftPresset) {
                    $('#ccfD20').focus()
                } else {
                    $('#ccfF20').focus()
                }
            }
            if (id === 'ccfD20') {
                if (shiftPresset) {
                    $('#ccfC20').focus()
                } else {
                    $('#ccfE20').focus()
                }
            }
            if (id === 'ccfC20') {
                if (shiftPresset) {
                    $('#ccfB20').focus()
                } else {
                    $('#ccfD20').focus()
                }
            }
            if (id === 'ccfB20') {
                if (shiftPresset) {
                    $('#ccfF19').focus()
                } else {
                    $('#ccfC20').focus()
                }
            }
            if (id === 'ccfF19') {
                if (shiftPresset) {
                    $('#ccfE19').focus()
                } else {
                    $('#ccfB20').focus()
                }
            }
            if (id === 'ccfE19') {
                if (shiftPresset) {
                    $('#ccfD19').focus()
                } else {
                    $('#ccfF19').focus()
                }
            }
            if (id === 'ccfD19') {
                if (shiftPresset) {
                    $('#ccfC19').focus()
                } else {
                    $('#ccfE19').focus()
                }
            }
            if (id === 'ccfC19') {
                if (shiftPresset) {
                    $('#ccfB19').focus()
                } else {
                    $('#ccfD19').focus()
                }
            }
            if (id === 'ccfB19') {
                if (shiftPresset) {
                    $('#ccfF18').focus()
                } else {
                    $('#ccfC19').focus()
                }
            }
            if (id === 'ccfF18') {
                if (shiftPresset) {
                    $('#ccfE18').focus()
                } else {
                    $('#ccfB19').focus()
                }
            }
            if (id === 'ccfE18') {
                if (shiftPresset) {
                    $('#ccfD18').focus()
                } else {
                    $('#ccfF18').focus()
                }
            }
            if (id === 'ccfD18') {
                if (shiftPresset) {
                    $('#ccfC18').focus()
                } else {
                    $('#ccfE18').focus()
                }
            }
            if (id === 'ccfC18') {
                if (shiftPresset) {
                    $('#ccfB18').focus()
                } else {
                    $('#ccfD18').focus()
                }
            }
            if (id === 'ccfB18') {
                if (shiftPresset) {
                    $('#ccfG16').focus()
                } else {
                    $('#ccfC18').focus()
                }
            }
            if (id === 'evcForShareholdersB23' && shiftPresset) {
                $('#evcForShareholdersB22').focus()
            }
            if (id === 'evcForShareholdersB22') {
                if (shiftPresset) {
                    $('#evcForShareholdersB21').focus()
                } else {
                    $('#evcForShareholdersB23').focus()
                }
            }
            if (id === 'waccF12') {
                if (shiftPresset) {
                    $('#waccE12').focus()
                } else {
                    $('#waccB25').focus()
                }
            }
            if (id === 'waccE12') {
                if (shiftPresset) {
                    $('#waccD12').focus()
                } else {
                    $('#waccF12').focus()
                }
            }
            if (id === 'waccD12') {
                if (shiftPresset) {
                    $('#waccC12').focus()
                } else {
                    $('#waccE12').focus()
                }
            }
            if (id === 'waccC12') {
                if (shiftPresset) {
                    $('#waccB12').focus()
                } else {
                    $('#waccD12').focus()
                }
            }
            if (id === 'waccB12') {
                if (shiftPresset) {
                    $('#waccF11').focus()
                } else {
                    $('#waccC12').focus()
                }
            }
            if (id === 'waccF11') {
                if (shiftPresset) {
                    $('#waccE11').focus()
                } else {
                    $('#waccB12').focus()
                }
            }
            if (id === 'waccE11') {
                if (shiftPresset) {
                    $('#waccD11').focus()
                } else {
                    $('#waccF11').focus()
                }
            }
            if (id === 'waccD11') {
                if (shiftPresset) {
                    $('#waccC11').focus()
                } else {
                    $('#waccE11').focus()
                }
            }
            if (id === 'waccC11') {
                if (shiftPresset) {
                    $('#waccB11').focus()
                } else {
                    $('#waccD11').focus()
                }
            }
            if (id === 'waccB11') {
                if (shiftPresset) {
                    $('#waccF10').focus()
                } else {
                    $('#waccC11').focus()
                }
            }
            if (id === 'waccF10') {
                if (shiftPresset) {
                    $('#waccE10').focus()
                } else {
                    $('#waccB11').focus()
                }
            }
            if (id === 'waccE10') {
                if (shiftPresset) {
                    $('#waccD10').focus()
                } else {
                    $('#waccF10').focus()
                }
            }
            if (id === 'waccD10') {
                if (shiftPresset) {
                    $('#waccC10').focus()
                } else {
                    $('#waccE10').focus()
                }
            }
            if (id === 'waccC10') {
                if (shiftPresset) {
                    $('#waccB10').focus()
                } else {
                    $('#waccD10').focus()
                }
            }
            if (id === 'waccB10') {
                if (shiftPresset) {
                    $('#waccF9').focus()
                } else {
                    $('#waccC10').focus()
                }
            }
            if (id === 'waccF9') {
                if (shiftPresset) {
                    $('#waccE9').focus()
                } else {
                    $('#waccB10').focus()
                }
            }
            if (id === 'waccE9') {
                if (shiftPresset) {
                    $('#waccD9').focus()
                } else {
                    $('#waccF9').focus()
                }
            }
            if (id === 'waccD9') {
                if (shiftPresset) {
                    $('#waccC9').focus()
                } else {
                    $('#waccE9').focus()
                }
            }
            if (id === 'waccC9') {
                if (shiftPresset) {
                    $('#waccB9').focus()
                } else {
                    $('#waccD9').focus()
                }
            }
            if (id === 'waccB9') {
                if (shiftPresset) {
                    $('#waccF8').focus()
                } else {
                    $('#waccC9').focus()
                }
            }
            if (id === 'waccF8') {
                if (shiftPresset) {
                    $('#waccE8').focus()
                } else {
                    $('#waccB9').focus()
                }
            }
            if (id === 'waccE8') {
                if (shiftPresset) {
                    $('#waccD8').focus()
                } else {
                    $('#waccF8').focus()
                }
            }
            if (id === 'waccD8') {
                if (shiftPresset) {
                    $('#waccC8').focus()
                } else {
                    $('#waccE8').focus()
                }
            }
            if (id === 'waccC8') {
                if (shiftPresset) {
                    $('#waccB8').focus()
                } else {
                    $('#waccD8').focus()
                }
            }
            if (id === 'waccB8') {
                if (shiftPresset) {
                    $('#waccG6').focus()
                } else {
                    $('#waccC8').focus()
                }
            }
            if (id === 'waccG6') {
                if (shiftPresset) {
                    $('#waccF6').focus()
                } else {
                    $('#waccB8').focus()
                }
            }
            if (id === 'waccF6') {
                if (shiftPresset) {
                    $('#waccE6').focus()
                } else {
                    $('#waccG6').focus()
                }
            }
            if (id === 'waccE6') {
                if (shiftPresset) {
                    $('#waccD6').focus()
                } else {
                    $('#waccF6').focus()
                }
            }
            if (id === 'waccD6') {
                if (shiftPresset) {
                    $('#waccC6').focus()
                } else {
                    $('#waccE6').focus()
                }
            }
            if (id === 'assumptionsB41') {
                if (shiftPresset) {
                    $('#assumptionsB40').focus()
                } else {
                    $('#assumptionsB42').focus()
                }
            }
            if (id === 'assumptionsB42') {
                if (shiftPresset) {
                    $('#assumptionsB41').focus()
                } else {
                    $('#assumptionsB44').focus()
                }
            }
            if (id === 'waccB5') {
                if (shiftPresset) {
                    $('#waccG4').focus()
                } else {
                    $('#waccB6').focus()
                }
            }
            if (id === 'waccB6') {
                if (shiftPresset) {
                    $('#waccB5').focus()
                } else {
                    $('#waccC6').focus()
                }
            }
            if (id === 'waccC6') {
                if (shiftPresset) {
                    $('#waccB6').focus()
                } else {
                    $('#waccD6').focus()
                }
            }
        }
    });

    // eventos relacionados a los inputs
    $('input').keyup(function (event) {
        var keyCode = event.keyCode;
        if (keyCode === 16) {
            shiftPresset = false;
        }
    }).keydown(function (event) {
        var keyCode = event.keyCode;
        if (keyCode === 16) {
            shiftPresset = true;
        }
        var numberKey = (keyCode > 47 && keyCode < 58) || (keyCode >95 && keyCode < 106);
        var directionKey = keyCode === 37 || keyCode === 39;
        var tabKey = keyCode === 9;
        var specialKey = keyCode === 46 || keyCode === 190 || keyCode === 8 || keyCode === 189 || keyCode === 109 || keyCode === 110;
        if (numberKey || specialKey || directionKey || tabKey) {
            var amount = $(this).val().replace(',','.');
            var present = 0;
            var count = 0;

            do
            {
                present = amount.indexOf(".", present);
                if (present !== -1) {
                    count++;
                    present++;
                }
            }
            while (present !== -1);
            if (present === -1 && amount.length === 0 && event.keyCode === 46) {
                event.keyCode = 0;
                return false;
            }

            if (count >= 1 && event.keyCode === 46) {

                event.keyCode = 0;
                return false;
            }
            if (count === 1) {
                var lastdigits = amount.substring(amount.indexOf(".") + 1, amount.length);
                if (lastdigits.length >= 2) {
                    event.keyCode = 0;
                    return false;
                }
            }
            return true;
        }
        else if (keyCode === 13) {
            startCalculateData();
        } else {
            event.keyCode = 0;
            return false;
        }
    }).change(function () {
        startCalculateData();
    }).focus(function () {
        $(this).select();
    });
});